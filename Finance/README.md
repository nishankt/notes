
1. Live below your means!!
2. Money for not luxury goods but to get freedom to do things - business, vacations, medical
3. What makes you really happy?

# College money
Assumptions:
 Starting point        : Jan 2019
 Private education cost: ~$45,000 pa
 Rate of fee increases : ~3% annually
Nandika  [2027-2031]
 [45000 x (1.03)^8  = 57000] + 58710 + 60470 + 62285 = $238,465
 8 * 12 = 96 months => 238465/96 = $2484 per month
 With 0.3% monthly interest => 238465 * (1.003 -1) / ((1.003)^96 - 1) = $2147 per month
 With 5.5% monthly interest => 238465 * (1.055 -1) / ((1.055)^96 - 1) = $77 per month
Ambalika [2032-2036]
 [45000 x (1.03)^13 = 66084] + 68066 + 70108 + 72211 = $276,469
 13 * 12 = 156 months => 276469/156 = $1772 per month
 With 0.3% monthly interest => 276469 * (1.003 -1) / ((1.003)^156 - 1) = $1392 per month
 With 5.5% monthly interest => 276469 * (1.055 -1) / ((1.055)^156 - 1) = $3.6 per month
Total
 For private: $3539 per month ($80 with 5.5%)
 For public : $1769 per month ($40 with 5.5%)

# Terms
Principal         = original amount borrowed
Simple interest   = interest => rate * principal     [P (1 + r * time)]
Compound interest = interest => rate * new principal [P (1 + r)^time]
APR               = Annual percentage rate
DPR               - Daily percentage rate (APR / 365). CC company charge DPR compound interest

# Debt
High rate method - pay off highest APR first, followed by next APR; optimal strategy
Snowball method - pay off lowest debt first; not optimal
## Bankruptcy
Chapter 7 - straight; repay by possesion; 10yrs on credit report
Chapter 13 - reorganization; negotiate loan rate/interest; to be paid within 3-5 yrs; 7yrs on credit report

# Retirement
## Traditional IRA (Individual Retirement Account)
<50 $5000, >50 $6000
Taxes deferred; early withdrawal has 10% penalty + taxes
No taxes within IRA for sale/purchase of stocks
Income tax eventually after 60yr on withdrawal. Still better than not putting in IRA
Penalty if not withdrawn before 70
## Roth IRA
Taxes not deferred; early withdrawal has no penalty or taxes on principal but 10% penalty and taxes on earnings
No taxes within Roth for sale/purchase of stocks
On withdrawal, no earning taxes after 60yr
Benefits only if planning to take out amount prior to 60yrs, else Traditional is better
## 401k
Similar to Traditional IRA but with higher limit ($18500)
put pre-tax money in; money grows in account untaxed; withdraw after 60yrs
Income tax on total dispersals
Penalty if not withdrawn before 70
Employer might match certain percentage and taken out of paycheck
In IRA, you have option to invest where. In 401k, whatever company fund manager decides
So, better than traditional IRA
## 401k rollover
When changing job, one can - 
1. leave it in previous fund
2. rollover to new company - if more matching or flexibility in new fund manager, keep in one place.
3. move to IRA - flexible
4. move to bank - pay penalty and taxes, not recommended

# Mutual Funds
Open-end MF: At the EOD, buy or sell more shares within MF
Closed-end: Stocks created only at beginning of MF, later each share can be traded in market but not created
ETF (Exchange traded fund): Combination of Open and closed MF

# Stock vs Bond
Stock - part owner of the compnay
Bond  - part lender of the company
Equity = Assets - Liabilities
Share book value = Total equity /# shares

# Rent vs Buying
~30% of total income (pre-tax) for total rent (+ util, amenities, repair, etc)
Rent: invest cash in account for higher return
Buy: deduct mortgage interest from income tax

# Mortage interest rates
30yr fixed - 5% pa for 30yr, fixed monthly payment (initially more interest and less principal)
15yr fixed - 4% pa for 15yr, fixed monthly payment (initially more interest and less principal)
5/1  ARM - adjustable rate mortage, 30yr term, fixed rate (3%) for 5yr, then rate changed for next year
10-yr treasury - 2% and changes annually
Higher the credit score, lower is interest rate
