
# Characters
  `\d` single digit
  `\w` single word char
  `\s` single whitespace (space, tab, newline, carriage return, vertical tab)
  `\D` single non-digit
  `\W` single non-word char
  `\S` single non-whitespace

# Quantifiers
  `.` any character
  `+` one or more
  `*` zero or more
  `?` one or none
  `{3}` exactly three times
  `{2,5}` two to five times
  `{2,}` two or more times

# Logic
  `[abc]` any of 'a' or 'b' or 'c'
  `[^abc]` not any of 'a' or 'b' or 'c'
  `^abc$` starts and ends with 'abc'
  `\b` word boundary (e.g. `\bword\b` will match ' word ' but not 'words')
  `(abc)` capture group match
  `\1` back reference to first captured group match

https://regexr.com/

# Examples

## IP
  ``([0-9]{1,3}.){3}[0-9]{1,3}`` # simple match, does't tests octects over 255
  ``\b(?:(?:2(?:[0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9])\.){3}(?:(?:2([0-4][0-9]|5[0-5])|[0-1]?[0-9]?[0-9]))\b`` # RFC based test
  ``\b(([a-fA-F0-9]{1,4}|):){1,7}([a-fA-F0-9]{1,4}|:)\b`` # for IPv6

## email
  ``[a-z0-9]+[_a-z0-9\.-]*[a-z0-9]+@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})`` # simple match
  ``[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?`` # RFC based match

## url
  ``([--:\w?@%&+~#=]*\.[a-z]{2,4}\/{0,2})((?:[?&](?:\w+)=(?:\w+))+|[--:\w?@%&+~#=]+)?``

## password validification
  ``^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$``  # validates 8+ chars, 1upper, 1lower, 1number, allow special chars

## find comma separated fields, excluding commas within double-quotes
  ``,(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))``

## phone number
  ``^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$``

## match text which doesn not contain 'text1' or 'text2'
  ``^(?:(?!text1|text2).)+$``

