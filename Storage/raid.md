
RAID can - improve performance, replicate data

# Linear or JBOD
Is not real RAID but all RAID controllers implement it.
JBOD concatenates blocks addresses of multiple drives to create single large virtual drive.
No performance benefit or redundancy.

# Level 0
Stripes data
Increases performance, but no redundancy.
```
      RAID 0
    +---------+
  | 1a |    | 1b |
  | 2a |    | 2b |
  | 3a |    | 3b |
```

# Level 1
Mirrors data
Slower but data redundant
```
      RAID 1
    +---------+
  | 1  |    | 1  |
  | 2  |    | 2  |
  | 3  |    | 3  |
```
# Level 1+0, 0+1
Mix RAID level 1 and 0, in different order
```
      RAID 1                           RAID 0
  +--------------+                +----------------+
RAID 0          RAID 0         RAID 1             RAID 1
+---+           +----+         +----+             +----+
1a  1b          1a   1b        1a   1a            1b   1b
2a  2b          2a   2b        2a   2a            2a   2b
 "mirror of stripes"                 "stripe of mirrors"
```

# Level 5
Stripes both data and parity.
Increases read performance but back write performance.
Better redundancy (protects against 1 disk failure)
At least 3 disks are needed, of which N-1 store data and 1 for parity
Can corrupt data (RAID 5 write hole) due to incremental parity.
Scrubbing (validating parity block in idle time) needs to be done via cron job.
```
           RAID 5
   +------+-----+------+
   1a    1b     1c     P1
   2a    2b     P2     2c
   3a    P3     3b     3c
   P4    4a     4b     4c
```

# Level 6
Same as 5 but with 2 parity, so it can withstand failure of 2 drives.
Requires at least 4 drives.
All issues of RAID5.
```
           RAID 6
   +------+-----+------+------+
   1a    1b     1c     P1     Q1
   2a    2b     P2     Q2     2c
   3a    P3     Q3     3b     3c
   P4    Q4     4a     4b     4c
```


# Linux's software RAID (mdadm)
ZFS and BTRFS support striping, mirroring, configurations similar to RAID 5 and 6.
Linux's `md` provides software RAID solution for other filesystems.

