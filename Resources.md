Best resource on various subjects

# Algorithms
[William Fiset (DFS/BFS in matrix)](https://www.youtube.com/c/WilliamFiset-videos/playlists)
[Intro to Grad Algo - UD401 (DP)](https://www.udacity.com/course/introduction-to-graduate-algorithms--ud401)

# OS general
[Duartes](https://manybutfinite.com/category/software-illustrated/)
OS: Three Easy Pieces (persistence, coherency)

# Memory
[Virtual Memory](https://youtu.be/qcBIvnQt0Bw?list=PLiwt1iVUib9s2Uo5BeYmwkDFUh70fJPxX)
[Linux page management](https://www.linkedin.com/pulse/linux-memory-management-part-1-gopakumar-thekkedath?trk=related_artice_Linux%20Memory%20Management%2C%20Part%201)
[Cache coherency](https://www.youtube.com/playlist?list=PLeWkeA7esB-OgNoVkE2lW2cVBxpDbu92h)
[Linux MMU](https://bitcharmer.blogspot.com/2020/05/t_84.html)

# process
[How ptrace works](https://blog.packagecloud.io/eng/2016/02/29/how-does-strace-work/)
[Guide to linux/x86 system calls](https://blog.packagecloud.io/eng/2016/04/05/the-definitive-guide-to-linux-system-calls/)

# Filesystems
VFS and Block I/O chapter from `Linux Kernel Development - Robert Love`

# Network
Internetworking With TCP/IP - Douglas Comer (whole book)
[How packets move through network](https://www.youtube.com/watch?v=rYodcvhh7b8&list=PLIFyRwBY_4bRF49oTwJVK4LqWTKuACRWc&index=2&t=0s)
[Linux network stack](https://blog.packagecloud.io/eng/2016/06/22/monitoring-tuning-linux-networking-stack-receiving-data/)
[Life of packet](https://www.youtube.com/watch?v=T5TvPRQFNoM)

# Virtualization
[KVM, x86](https://binarydebt.wordpress.com/2018/10/14/intel-virtualisation-how-vt-x-kvm-and-qemu-work-together/)

# Courses
[MIT 6.828 (Operating Systems) Labs](https://pdos.csail.mit.edu/6.828/2018/labs)
[MIT 6.824 (Distributed systems)](https://pdos.csail.mit.edu/6.824/)
[Stanford CS144 (Network) Labs](https://cs144.github.io/)
[Harvard CS50 problem sets](https://cs50.harvard.edu/summer/2020/psets/)

