* [Book](https://www.amazon.com/gp/reader/0136554822#reader_B081ZDXNL3)

* [A thorough introduction to eBPF](https://lwn.net/Articles/740157/)
* [The art of writing eBPF programs: a primer](https://sysdig.com/blog/the-art-of-writing-ebpf-programs-a-primer/)
* [Dive into ebpf](https://qmonnet.github.io/whirl-offload/2016/09/01/dive-into-bpf/)
* [bpf, ebpf, xdp, bpfilter](https://www.netronome.com/blog/bpf-ebpf-xdp-and-bpfilter-what-are-these-things-and-what-do-they-mean-enterprise/)
* [ebpf brendan](http://www.brendangregg.com/ebpf.html)
* [cilium ebpf xdp](https://cilium.readthedocs.io/en/doc-1.0/bpf/)
* [bpf comes to firewalls](https://lwn.net/Articles/747551/)
* [ebpf hop distacce, manually write assembly](https://blog.cloudflare.com/epbf_sockets_hop_distance/)
* [kernel tracing with ebpf](https://media.ccc.de/v/35c3-9532-kernel_tracing_with_ebpf)
* [Learn ebpf tracing](http://www.brendangregg.com/blog/2019-01-01/learn-ebpf-tracing.html)
* [Brendan Gregg's ebpf](http://www.brendangregg.com/ebpf.html)
* [An eBPF overview](https://www.collabora.com/news-and-blog/blog/2019/04/15/an-ebpf-overview-part-2-machine-and-bytecode/)
* [ebpf compiler collection](https://lwn.net/Articles/742082/)
* [ebpf can't count](https://blog.cloudflare.com/ebpf-cant-count/)

# XDP
* [A brief introduction to XDP and ebpf](https://blogs.igalia.com/dpino/2019/01/07/a-brief-introduction-to-xdp-and-ebpf/)
* [IOVisor XDP](https://www.iovisor.org/technology/xdp)
* [XDP intro](https://www.youtube.com/watch?v=lpJk_HcCLnQ)

