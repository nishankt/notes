https://stackoverflow.com/questions/39185134/how-are-user-level-threads-scheduled-created-and-how-are-kernel-level-threads-c/39185831#39185831

# Threads vs Process
+ Threads share resources - memory space, code, 
+ But has own - stack, IP, regs, 
+ CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SYSVSEM
+ Due to stack, number of threads on system is limited (~1MB per thread)

# Threading models

## 1:1
+ NPTL was evolved where kernel knew about threads which were just process with shared resources among themselves
+ All OS use this (pthreads too)
+ Kernel can better schedule each thread, which get equal share of CPU resources
+ Not much fine grained control from user's perspective
+ But now syscall overhead for each threading API. e.g. pthread_mutex_lock() would do syscall even just to find lock taken, instead of few instructions.
+ Windows implementation does provide partial-user space APIs for things like checking lock status.

## N:1
+ This was orignal Solaris model, also knows as LWP (light weight process)
+ Kernel was not aware of threads and userspace master process managed all threads by itself
+ Timeslice for this process would get further divided among threads, which was undesirable.
+ If process was low priority, all threads got lower priority and risked starvation.
+ Threads must explictly yield to other threads, i.e. play fair
+ If one thread does a blocking syscall, then all the threads are blocked. To mitigate this, blocking call can be wrapped with a check to see if call would be blocked. If so, it should wait until it's ok to issue the call (which can be implemented via binary semaphores). This extra wait obviously adds overhead.

## N:M
+ N userspace threads per each of M kernel threads
+ Userspace scheduler can assign kernel to thread to different user threads based on priority, or other critteria
+ Green threads, goroutines, fibres
+ Here blocking thread need not block all other threads, depending on how many kernel threads are created.
+ If only 1 kernel thread or too few kernel thread created, still wrapper solution from N:1 should be used, else it can create deadlock. However, too many kernel thread take system resources (stack space particularly)
+ One issue with N:M is that user space and kernel scheduler policies might be competing or negating each other.

# Thread stacks

# User level scheduler
saving/restoring context for each thread

# Thread APIs


- [Coroutines in C](https://www.chiark.greenend.org.uk/~sgtatham/coroutines.html)
- [Implementing simple cooperative threads in C](https://brennan.io/2020/05/24/userspace-cooperative-multitasking/)
