https://www.internalpointers.com/post/gentle-introduction-multithreading
https://blog.feabhas.com/2009/09/mutex-vs-semaphores-%E2%80%93-part-1-semaphores/
http://moodycamel.com/blog/2014/detailed-design-of-a-lock-free-queue

# thread-safe
A piece of code is thread-safe if it functions correctly during simultaneous execution by multiple threads. Following are ways to achieve thread-safety -

## re-entrant
+ Code which can be partially executed by one task, then reentered by another task, and then resumed by original task without change of state. 
+ This requires the saving of state information in variables local to each task, usually on its stack, instead of in static or global variables.

## Mutual exlusion
+ Access to critical section is serialized.

## Thread local storage
+ Variables are localized so that each thread has its own private copy.
+ Even though overall code is renentrant, each thread has its own copy.

## atomic operations
+ Shared data are accessed by using atomic operations which cannot be interrupted by other threads.

# deadlock
+ recursive deadlock - same process tries to take same lock
+ through task death - lock owner died without releasing lock
+ livelock - e.g. ping pong between locks
+ priority inversion - higher prio task waiting on lock held by lower prio task. This can happen if L had lock (which will get released if H tries to take it). But in the mean time M comes in (and has no need for lock). L gets preempted as M has higher prio. Now if H comes in (preempting M) and tries to take lock, L cannot release it (since it cannot be scheduled due to lower prio). Avoided via- 
++ disabling interrupt - so no one can preempt L and it can finish CS. At the end of CS, among two waiting process H and M, H will be picked.
++ priority ceiling - raise the priority of L to lock's priority which would be at least equal to H
++ priority inheritance - only if H tries to acquire lock, raise L's priority to H until CS is done
++ https://cmdlinelinux.blogspot.com/2013/12/priority-inversion-how-to-avoid-it.html

lock-free vs wait-free
+ Use inherent atomic operations but one task might have to try again and again (i.e. wait)
+ No wait either.

# Synchronization primitives

## spinlock
+ lock, unlock
## mutex
+ recursive, reader/writer
+ lock, unlock

## semaphore
+ counting sem, binary sem
+ acquire, release

## condition variable
+ Always hand-in-hand with mutex
+ wait, notify_one, notify_all
+ spurious wakeups (sleeping thread wakes up due to other condition)
+ CV should check for spurious wakeups

## Mutex vs Semaphore
+ Mutex looks like binary semaphore but it is not
+ Mutex is locking mechanism, semaphore is signalling mechanism
+ Mutex to be unlocked by same task which locked it
+ Semaphore could be unlocked by any process (after which both will be runnable)

