
Zero-copy is a concept in networking where buffer is kept in place and not moved around or copied.
If scatter/gather is supported then header data needs to be placed before payload data. Else, headers can be anywhere in memory.
Zero-copy is not useful if scatter/gather is supported by NIC.

https://www.linuxjournal.com/article/6345?page=0,1
https://stackoverflow.com/a/18346526
https://blogs.oracle.com/linux/zero-copy-networking-in-uek6

# Zero-copy send

## regular read and send
```
read(file, buf, len);
write(sk, buf, len);

        dma                                                                 dma
[drive] ---> [kernel buffer] ---> [user buffer] ---> [socket/kernel buffer] ---> NIC
      copy #1               copy #2           copy #3                      copy #4
```

Too many copies, low performance.

## mmap
```
buf = mmap(file, len);
write(sk, buf, len);

        dma           user also gets access              dma
[drive] ---> [kernel buffer] ---> [socket/kernel buffer] ---> NIC
      copy #1               copy #2                     copy #3
```

This was good if file was static. However, if file gets truncated or is dynamic there will be SIGBUS and other crashes.
Instead of installing signal handlers, better is to get lease on file `fcntl(fd, F_SETSIG, RT_SIGNAL_LEASE); fcntl(fd, F_SETLEASE, l_type);`. Now if file changes underneath, app will get a signal 

## sendfile
```
sendfile(sk, file, len);

        dma                                              dma
[drive] ---> [kernel buffer] ---> [socket/kernel buffer] ---> NIC
      copy #1               copy #2                     copy #3
```
If underlying file gets truncated, sendfile will only send whatever it can and return that many bytes as result.
However, if NIC supports gather operation then kernel can simply post descriptors which NIC can use to DMA via scatter/gather. This further reduces another copy in kernel.
```
        dma           only post desc in socket buffer    dma
[drive] ---> [kernel buffer] ...> [socket/kernel buffer] ---> NIC
      copy #1                                           copy #2
```
TCP offload is one example which when supported in hardware can gather data from multiple locations and then attach TCP headers too.

## MSG_ZEROCOPY
Kernel introduced new socket option (SO_ZEROCOPY) and send flag (MSG_ZEROCOPY).
```
setsockopt(fd, SOL_SOCKET, SO_ZEROCOPY, &one, sizeof(one));
send(fd, buf, sizeof(buf), MSG_ZEROCOPY);
```
Once buffer has been transmitted, application is notified via socket MSG_ERRORQUEUE that buffer is available for reuse.
Due to memory pinning overhead, this method is effective only if writes are larger than 10K.
Supported only for TCP, UDP, RAW, and RDS TCP sockets.

# Zero-copy receive

Receive is much harder since it requires data buffer to be page aligned for it to be mmap'd. Due to proto headers and smaller MTU, alignment becomes challenging.

## packet mmap

## AF_XDP
Can be used for both rx and tx.

