
The home WiFi router is two devices in one - a switch and a router.

```
                               +-----+
+------------------------------| WAN |--------------------------------+
|Home WiFi                     +-----+                                |
|                                 | a.b.c.d                           |
|                                 |                                   |
|                              [ NAT ]                                |
|                                 |                                   |
|                            [ firewall ]                             |
|                                 |                                   |
| +-------------+            +----------+                             |
| | DHCP server |------------|  Router  |                             |
| +-------------+            +----------+                             |
|                                 | w.x.y.z                           |
|                         +------------------+      +--------------+  |
|                         |     Switch       |------| Access point |  |
|                         +------------------+      +--------------+  |
|                           |    |    |    |                          |
+--------------------------[1]--[2]--[3]--[4]-------------------------+
```
The switch connects all devices in local LAN together.
The router connects the switch to ISP (WAN)

