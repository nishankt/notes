
# L3 switch
A switch is classified as L3 switch if it understands IP packet.

# Forwarding
Can be direct (on same network) or indirect (via next hop).
Forwarding table contains | network | port |

The algorithm for forwarding is -
  1. extract destination address (D) from datagram
  1. if table contains host-specific entry for D, forward datagram to next hop specified for this entry
  1. if network prefix of D matches prefix of any directly connected networks, send datagram directly to D
  1. if network prefixes listed in table match network prefix of D, send datagram to next hop specific for this prefix
  1. if table has default route, forward datagram to next hop specified for default route
  1. declare forwarding error

In practice, a unified lookup scheme is used in forwarding table. Each entry in table needs
  1. IP address which gives destination for entry           (A)
  1. Mask which tells how many bits of IP address to match  (M)
  1. "direct-deliver" or IP address of next hop router      (R)
  1. interface to use for forwarding                        (I)
In such tables, entries are listed with longest prefixes (larger masks) first. Also mask /0 means all destination addresses will match i.e. default route. If no default route is listed, then forwarding error is declared.
```
destination = D
for each entry in table:
  prefix P = D & M
  if P matches A:
    next hop = (direct-delivery ? D : R)
    find next hop's h/w MAC via ARP/NDP
    send frame on interface I
    stop
forwarding error
```

The loopkup table can be sorted array (low end routers) or trie based (high-end). Usually in high end routers, hardware assist in form of TCAM is utilized.
