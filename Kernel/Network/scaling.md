# RSS - receive side scaling
For multi-queue NICs, packets can be hashed using proto/port/addr and then each stream gets sent to a specific queue. This queue can be tied to a CPU, taking advantage of locality.

# RPS - receive packet steering
If NIC does not support multi-queue, software side hashing can be done. Each inbound packet is hashed and ISR maps the interrupt to a specific CPU.

# RFS - receive flow steering
Same as RPS but affinity for flow to stick to a socket.

# Accelerated RFS
RFS in hardware. NIC is aware of flow and accordingly treats the packet.

# XPS - transmit packet steering
With NIC having multiple transmit queues.

