# Network Time Protocol
Client requests server for precision time. Later is calculates propogation time and fixes it's clock to sync.

# Precision calculation
```
server            t1 .... t2
                  ^        \
                 /          v
client          t0          t3
```
time offset = ((t1 - t0) + (t3 - t2)) / 2
However, Linux is not RTOS, so even after writing t2 on packet and doing `send()`, packet is not immediately sent and this delay is unaccounted. Also, is ARP request was needed, then this time goes unaccounted as well.

# Network design
```
stratum 0        [node]       [node]       [node]      precision clocks - GPS sats, atomic clocks
                   |            |            |
stratum 1       [server] --- [server]     [server]     primary time servers
                 / |   ________| \  _______/ | \
                /  |  |        |  \|         |  |
stratum 2      [s] [s] [s]     [s] [s]       [s] [s]   can be upto stratum 15
                .  .  .         . .           . .
```
Each client has a conf file, where it stores which servers to reach out. All nodes use Bellman-Ford shortest path spanning tree.


# Message format
Can use either 32bit seconds (136 years) format for 64bit seconds.
```
      63                               31                            0
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |          Seconds              |           Fraction            | Short format
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                            Seconds                            | Long format
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                            Fraction                           |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

       0                   1                   2                   3
       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |LI | VER |Mode |    Stratum     |     Poll      |  Precision   |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                         Root Delay                            |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                         Root Dispersion                       |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                          Reference ID                         |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      +                     Reference Timestamp (64)                  +
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      +                      Origin Timestamp (64)                    +
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      +                      Receive Timestamp (64)                   +
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      +                      Transmit Timestamp (64)                  +
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      .                                                               .
      .                    Extension Field 1 (variable)               .
      .                                                               .
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      .                                                               .
      .                    Extension Field 2 (variable)               .
      .                                                               .
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                          Key Identifier                       |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      |                            dgst (128)                         |
      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                            Packet Header Format
LI (leap indicator) - 0=> no warning, 1=> last min of day has 61s, 2=> last min of day has 59s, 3=> unknown
MODE = Symmetric, Broadcast, Client/Server
Stratym = 0=> invalid, 1=> primary, 2-15=> secondary, 16=> unsynchronized, 17-255=> reserved
```

# ntpd
ntpd is a daemon for Linux which can be used as server or client.
`/etc/ntp.conf` is used for configuring ntpd. It saves clock's offset frequency in wherever driftfile option specifies (e.g. /var/lib/ntp/ntp.drift)
`ntpq -pn` provdes details on NTP servers, offsets, jitter, delay etc

# Troubleshooting
1. Is client behind firewall?
1. Is config misconfigured
1. Is daemon allowed to modify system clock?

