
# Address Resolution Protocol (ARP - ethertype 0x8034)
Used to find hardware MAC address of host, whose IP address is know.

As per RFC, can be used by any higher level protocol with any low-level addressing scheme.
But is primarily used with IPv4 and 6-octect (48bit) OUI MAC.
IPv6 can do direct addressing where MAC address gets embedded in 128bit IPv6 address.

Before sending higher protocol packet, MAC address of target is needed for L2 layer.
ARP broadcasts a frame on local network which has it's IP and MAC address, as well as IP address of target host.
All other hosts discard frame but target host replies with it's MAC addresss. Target host also saves sender's IP to MAC mapping.
On boot up, host usually sends a gratutious ARP packet with it's own IP and MAC address. This also checks if there is any IP or MAC dup on local network, as dup host will respond.
A host does not send ARP frame unless it is needed, i.e. late binding approach.

# ARP caching
All the responses of ARP are cached for certain duration on local host.
ARP also utilizes early revalidation of cache, where a shorter timer than cache expiry is set. ARP entry is revalidated on early revalidation timer expiry, while other stream is still going on. This avoids jitter in the stream, which would have to stop and wait on ARP cache expiry if it was only one timer.
L3 switches also create local ARP cache. L3 switches keep listening to ARP frames on wire and build their database. When a host sends ARP request for a target, switch intercepts the frame (i.e. doesn't fwd it on it's other ports) and responds on behalf of target with target's MAC and IP.


# Message format
```
S_HA = sender's hardware address, T_HA = target's hardware address
S_L32 = sender's IPv4 address
S_NID = sender's network ID
        0        7        15       23       31
        +--------+--------+--------+--------+
        |    HW TYPE      |     PROTO TYPE  |
        +--------+--------+--------+--------+
        |  HLEN  |  PLEN  |      OPERATION  | OPs = ARP request, ARP response, RARP req, RARP resp
        +--------+--------+--------+--------+
        |          S_HA   (bytes 0-3)       |
        +--------+--------+--------+--------+
        | S_HA (bytes 4-5)|S_L32 (bytes 0-1)|
        +--------+--------+--------+--------+
        |S_L32 (bytes 2-3)|S_NID (bytes 0-1)|
        +--------+--------+--------+--------+
        |         S_NID (bytes 2-5)         |
        +--------+--------+--------+--------+
        |S_NID (bytes 6-7)| T_HA (bytes 0-1)|
        +--------+--------+--------+--------+
        |         T_HA (bytes 3-5)          |
        +--------+--------+--------+--------+
        |         T_L32 (bytes 0-3)         |
        +--------+--------+--------+--------+
        |         T_NID (bytes 0-3)         |
        +--------+--------+--------+--------+
        |         T_NID (bytes 4-7)         |
        +--------+--------+--------+--------+
```

# Reverse Address Resolution Protocol (RARP - ethertype 0x8035)
Reverse ARP is used many times for booting diskless systems. On boot, system sends out RARP message which contains it's hardware MAC. A server on local network will have pre-configured database with MAC to IP mapping. It will respond to RARP request with sender's IP address. Sender from there on uses this IP address for itself.
While RARP is not used for diskless system anymore, it is used in datacenters when VM migrates from one host to another. It sends RARP frame to ethernet switch, so switch can update it's tables.


# IPv6 NDP (Neighbor Discovery Protocol)
IPv6 instead of ARP, uses NDP. 
NDP uses early binding and discovers neighboring nodes at startup itself. From then on, it continously checks status of its neighbors.
