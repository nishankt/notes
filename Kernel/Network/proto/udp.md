
While IP is host-to-host or hop-by-hop protocol, UDP/TCP are end-to-end protocol.
UDP uses port number to identify applications. TCP uses endpoints (host + port)
Every port has a queue (backlog) associated with it.

# Message format
```
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |    Source port (optional)     |          Destination port     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |      Message length           |      Checksum (optional)      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                           Data ....(64K)                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```
In IPv6 checksum is required but is optional in IPv4. So how to ensure integrity of the datagram? using pseduo-header!
Pseudo-header take SA and DA from IP and attaches a pseduo-header in front of UDP (even TCP) segment. The format of pseudo-header is
```
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                   Source IP address                           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                  Destination IP address                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Zero         |  Protocol     |         UDP length            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```
New checksum is calculated using pseduo-header, UDP header and UDP payload.
This newly calculated checksum gets placed in `Checksum` field of UDP header before passing on to upper layer.
Note that pseudo-header is only locally attached, it is never placed on the wire.


```c
//client                    server
getaddrinfo()               getaddrinfo()
socket()                    socket()
                            bind()
sento()                     recvfrom()
recvfrom()                  sendto()
close()                     close()
```
recvfrom() is blocking call.
