# OSI model
7 layered model.


# TCP/IP model
Could be 5 or 4 layered.



# Actual packet usage
```
   L1    L2      L3     L4     L5
              +- ARP
              |
              |      +- UDP  - [DNS, DHCP, SNMP, TFTP, NTP]
              |      |
   PHY - MAC -+- IP -+- TCP  - [BGP, HTTP, SMTP, Telnet, FTP, SSH]
              |  .   |
              |  .   +- OSPF
              |  .
              +- ICMP (even though L2, uses IP)
```
