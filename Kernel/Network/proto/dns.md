
Hostname to IP address
/etc/hosts.txt could do that but not scalabale

DNS is a distributed database which maps domain names to IP addresses. It is also useful to provide email routing info and handle aliases.
DNS is divided in different domain nodes - top level being root. Each domain node has a label consisting of 0-63 bytes. Top level domain is empty label. "com", "org" etc are other labels.
gTLD = generic Top Level Domains (com, mil, gov, info, etc)
ccTLD = country code TLD (in, uk, de, etc)

# Size limits
labels = 63b
names  = 255b
TTL    = INT_MAX
Message= 512b of UDP

# Root servers
Top level servers, where DNS query resolution starts from.
Each root zone file contains records of authoritative servers for TLDs.
There are 13 root servers - [a-m].root-servers.net
[Why 13?](https://miek.nl/2013/november/10/why-13-dns-root-servers/) Early DNS was limited to UDP on IPv4. IP governs that host should be able to reassemble frames of 576b or less. With DNS's header, question, answer and additional sections, it roughly gets contained with 512b.

With DNS extension, TCP can be used if records dont fit in 512bytes.

root domain is managed by ICANN, "com" by Verisign, etc. Since, TLD cannot store all information about all nodes, it delegates responsibility to other TLDs, .e.g. "com" for all domains with ".com". gTLDs further delegate to other sub-domain managers.
Zones are subtrees within domain for which naming authority has been delegated. Name server stores information about the zone.

```
                         [root]
                        /
[client] -> [resolver] <---> [com]
                        \
                         [xyz.com]
```

Resolver is a library or utillity which initiates DNS query from client to resolve a hostname.
Forwarder sits after resolvers and it's purpose is to take request from resolver and do resolver's job of recursively querying. It's primarily used nowdays internal to a firewalled network. This way it builds on large cache as well as one controller entry and exit point.

BIND name server for UNIX family.
gethostbyname() is just front-end for OS's DNS resolvers

# Resource Records (RR)
In DNS format, which is binary. In presentation format, which is text.
`owner-name    [TTL]     [class]    type      RDATA`
"type" can be
  A (IPv4 addr)
  AAAA (IPv6 addr)
  PTR (reverse map IP addr to domain name)
  CNAME (alias or canonical name) If resolver finds CNAME record instead of A record, it'll query using CNAME to fetch A record.
  MX (mail exchange record) contains mail server's domain name as well as priority for this server. [owner-name <TTL> IN MX <16-bit preference> <mail exchanger>]
  SRV (general purpose service record) can work with any service [_svc._proto.domain-name <TTL> IN SRV <16-bit prio> <16-bit weight> <port> <target i.e domain name>]
  TXT (text or comment record) Miscll data or comments. [owner-name [TTL] IN TXT "string" ["string" ..]
  SOA (used only by DNS itself, not for app) summary info on zones
  NS (used only by DNS itself, not for app) specify authoritative NS for a zone

# Glue Records
Needed to prevent circular references - if NS cannot resolve without resolving the domain itself is responsible for.
e.g. domain.com has name servers ns1.domain.com ns2.domain.com. But DNS is still resolving domain.com so it cannot resolve ns1.domain.com
So glue record is added to parent DNS server (in this case .com gTLD server), which is additional A record that are returned with DNS response.
One needs to contact name registrar to add glue records as they need to be added to parent.


# gethostbyname()
Implemented in glibc, it performs DNS lookups.
It initially looks into /etc/hosts.conf file to see if any direct entries are available.
Then it uses /etc/resolv.conf to find out about DNS servers to start full DNS query.
