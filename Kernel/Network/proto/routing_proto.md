

# Forwarding Information Base (FIB)
Routing protocols work independently of forwarding mechanism.
Routing protocol software learns about destinations, shortest paths, and pass information to it's peer' routing protocol software.
Instead of adding destinations and shortest paths to forwarding table, routing software creates Forwarding Information Base (FIB).
FIBs will contain extra information about routes such as how old data is, source of routing info, if path is overriden in policy.
Routing table gets installed based on FIB and checking with policy (which migiht govern excluding some routes).
So shortest path found by routing protocol might not make it to forwarding table.

To create FIB, routing protocol use Distance-Vector (Bellman Ford) algowithm to exchange information and update own FIB.
But Distance-Vector does not scale well, so Link-State (Shortet Path First) type of algorithms are used.

# Autonomous Systems
However, not all routers can be part global internet. So logical groups are created with participating routers for a group.
For purpose of routing, group of networks and router managed by an administrative authority is considered autonomous system.
To facilitate discovery of AS by routing protocols, IANA assigns Autonomous System Numbers.
AS can be any org managing large network like University, Corporations, ISP, etc.

# Exterior Gateway Protocols (EGP)
Each AS configure one or more routers to communicate with other AS routers.
Such routers convey reachability to their peers via EGP.
Technically, EGP is not routing protocol since it only advertises reachability and not routing information.

## Border Gateway Protocol (BGP)
BGP-4 is a EGP widely used.
Usually, the assigned router in one AS talking to another is placed on the edge of AS network, hence they are called border gateway or border routers.
Since BGP is concerned with reachability, it uses path-vector algorithms.
BGP also needs to support inter-AS communication and coordination among border routers.

Two BGP peers establish TCP connection to communicate.
Each side sends positive or negative reachability information, i.e. what destinations can be reached or not reachable anymore.
BGP message types are - OPEN (initiate connection), UPDATE (advertise route reachability), NOTIFICATION (resp to incorrect message), KEEPALIVE (actively test peer connection), REFRESH (request readvertisement from peer)

# Interior Gateway Protocol (IGP)
Used for routing information within AS. These routers talk among themselves within a AS and then convey information to border router.

## Routing Information Protocol (RIP) - uses distance-vector
## Open Shortest Path First (OSPF) - uses link-state
## IS-IS (intermediate systems) - similar to OSPF
