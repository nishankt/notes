
DHCP is how a client which does not know its IP, gets IP address.
DHCP is successor to BOOTP which was used to replace RARP.

When client sends the DHCP request, it does a broadcast. DHCP server in network see this broadcast and can either -
1. save arp address of client and then send direct frame to client with it's IP, or
1. broadcast the reply, which client uses to extract it's IP
Notice, server cannot sent ARP request since client does not know it's IP yet.

Note that DHCP's address assignment is temporary (leased until renewal)

# Message format
```
   0                   1                   2                   3
   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     op (1)    |   htype (1)   |   hlen (1)    |   hops (1)    |
   +---------------+---------------+---------------+---------------+
   |                  TRANSACTION ID (4)                           |
   +-------------------------------+-------------------------------+
   |           secs (2)            |           flags (2)           |
   +-------------------------------+-------------------------------+
   |                 CLIENT IPv4 ADDRESS (4)                       |
   +---------------------------------------------------------------+
   |                  YOUR IPv4 ADDRESS (4)                        |
   +---------------------------------------------------------------+
   |                  SERVER IPv4 ADDRESS (4)                      |
   +---------------------------------------------------------------+
   |                  ROUTER IPv4 ADDRESS (4)                      |
   +---------------------------------------------------------------+
   |                                                               |
   |                CLIENT HARDWARE ADDRESS (16)                   |
   |                                                               |
   +---------------------------------------------------------------+
   |                                                               |
   |                   SERVER HOSTNAME (64)                        |
   +---------------------------------------------------------------+
   |                                                               |
   |                 BOOT FILE NAME   (128)                        |
   +---------------------------------------------------------------+
   |                                                               |
   |                          options (variable)                   |
   +---------------------------------------------------------------+
```
Client tries to fill as much info as possible and leaves others as 0. A responding server will fill in "your IPv4 addres" in response.
There are over 100 options including DHCPDISCOVER, DHCPOFFER, DHCPREQUEST, DHCPACK, etc.

For diskless clients, DHCP request will also carry a generic name for identify which image it wants. Server has a map of these names to file and location of images.
The server on such query responds (in BOOT FILENAME) with FQDN of the image, which client next downloads (via tftp or such) and boots from it.
If BOOT FILENAME is all 0 then a default image location is sent.

# DHCP timeline for new address
```
               Server          Client          Server
            (not selected)                    (selected)
                  v               v               v
                  |               |               |
                  |     Begins initialization     |
                  |               |               |
                  | _____________/|\____________  |
                  |/DHCPDISCOVER | DHCPDISCOVER  \|
                  |               |               |
              Determines          |          Determines
             configuration        |         configuration
                  |               |               |
                  |\             |  ____________/ |
                  | \________    | /DHCPOFFER     |
                  | DHCPOFFER\   |/               |
                  |           \  |                |
                  |       Collects replies        |
                  |             \|                |
                  |     Selects configuration     |
                  |               |               |
                  | _____________/|\____________  |
                  |/ DHCPREQUEST  |  DHCPREQUEST\ |
                  |               |               |
                  |               |     Commits configuration
                  |               |               |
                  |               | _____________/|
                  |               |/ DHCPACK      |
                  |               |               |
                  |    Initialization complete    |
                  |               |               |
                  .               .               .
                  .               .               .
                  |               |               |
                  |      Graceful shutdown        |
                  |               |               |
                  |               |\ ____________ |
                  |               | DHCPRELEASE  \|
                  |               |               |
                  |               |        Discards lease
                  |               |               |
                  v               v               v
```
