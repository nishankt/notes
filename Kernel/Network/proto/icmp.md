
Even though ICMP uses IP protocol as if it were higher level protocol, ICMP is integral part of IP.
`| Frame header | IP header | ICMP |`

Internet Control Message Protocol allows routers to send control or error messages back to the source of datagram.
ICMP messages are usually not delivered to app but lower layers handle it.


# Message format
```
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |     Code      |          Checksum             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Rest of the header                         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                        data section                           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```
The message format differs based on type. Because there can be different types of ICMP messages, type field is needed.
Type can be `echo reply (0)`, `dst unreachable (3)`, `redirect (5)`, `request (8)`, `time exceeded (11)`, `timestamped request (13)`, `timestamped reply (14)`.
Code further specifies the type message. e.g. For type 3 (unreachable), code 0 = n/w unreachable, 1 = host unreachable, 2 = protocol unreachable, etc
While request will have header and data section, usually response will have unused header section and IP + 64b of original datagram in data section.

In ICMPv6, first 128 codes are error, and rest are control messages.

# Ping
Sends ICMP echo request messages (type 8, code 0).
Destination, if alive, sends echo reply message (type 0)
The TTL field in IP datagram is set to 62 hops.
Origin timestamp is recorded in data section. After receiving packet back, RTT is calculated based on timestamp added in data section.

# Traceroute
Sends 3 UDP datagrams which increasing TTL towards the destination.
Traceroute relies on "time exceeded" and "destination unreachable" error codes.
At each node where TTL becomes 0, packet is dropped and ICMP message (type 11, code 0) is sent to origin.
3 datagrams are used to calculating RTT (first one might be outlier due to ARP).
Once packet reaches destination, ICMP error cannot be sent back (as there is no error). Instead when original UDP datagram was created, an unsupported destination port was used (33434 to 33534). So when destination gets packet, it discards it with "destination unreachable" (type 3, code 3) error.
Traceroute prints "*" if it does not receive response within 5s, and then tries next hop.

Another alternative to Traceroute is `mtr` which is more graphical.
