
TCP provides following control -
  1. Flow control - receiver buffer should not overrun
  1. Error control - detect segment corruption, loss
  1. Congestion control - adjust for network congestion

# Flow control

## Window sizes
How to avoid overrunning buffer? receiver should advertize space it has left in its receiving buffer.
SEQ = starting sequence number of segment being sent from sender
ACK = next expected sequence number from sender (which means everything upto ACK-1 is recieved)
WIN = space left in receiver's buffer

Let's say receiver had 4k buffer for receiving, then -
```
S --> [2K data,  seq=0] ----> C [4K empty] -> [2K empty]
S <-- [ack=2K,  win=2K] <---- C
S --> [2K data, seq=2K] ----> C [2K empty] -> [0 empty]
S <-- [ack=4K,   win=0] <---- C
                                app reads 2k from buffer
S <-- [ack=4K,  win=2K] <---- C [0 empty]  -> [2K empty]
S --> [1K data, seq=4K] ----> C [2K empty] -> [1k empty]
S <-- [ack=5K,  win=1K] <---- C
```
## Silly Window Syndome
If either reciever consumes data slowly, or sender sends slowly, or both, segments sent are very small in size and hence, network is underutilized.
Nagle proposed solution (for slow sender) that sender should wait for ACK from receiver. This gives time for sender to coalesce more data before sending.
Clark proposed solution (for slow receiver) to send ACK asap but announce window size 0 until half or more receive buffer is available.
Delayed ACK is another way for slow receiver to fix Silly window Syndome. This also reduces traffic.


# Error control

## Checksum
For every segmenti, 16bit checksum is added.

## Acknowledgement
Cumulative ACK - send one ack for cumulative segments, ignoring out-of-order segments.
Selective ACK - SACK reports additional info to sender. Reports upto 4 blocks of segment (which could imply lost or dup segments). ACK bit is still set and SACK data is added at the end of the TCP header.

## Retransmission
1. after RTO (retranmission time-out) expires for sent segment (i.e. ACK was not received).
1. after 3 dup ACKs packets are considered to be lost packet and retransmission is done (aka Fast retransmit, TCP Reno)

## Out-of-order
With SACK, out-of-order segments are not discarded but also not delivered to app.

# Congestion control

While receive window is governed by receiver and hence concerned with receiver overflow, network too can get overwhelmed.
Congestion window is maintained by sender side and actual window size becomes `min(rwnd, cwnd)`
Congestion handling policy is based on 3 phases - slow start, congestion avoidance, and congestion detection.

## Slow start (exponential increase)
 Initially, congestion window is 1 segment which is maximum MSS initialized by receiver.
 When ACK is returned by the receiver, cwnd adds one more MSS to cwnd for each ACK.
 After first ACK, cwnd becomes 2. Post sending these 2 segments, receiver returns 2 ACKs.
 This time receiver adds 2 more MSS to cwnd, making total to 4. So forth, cwnd keeps doubling.

## Congestion avoidance (additive increase)
 If cwnd will keep increasing exponentially, eventually there will be congestion.
 To avoid congestion, as size of cwnd reaches slow start threshold (ssthresh), additive phase begins.
 For each received 'window' (not each MSS), cwnd is increased by one. Thus increasing slowly after ssthresh.

## Congestion detection (multiplicative decrease)
 When slow start keeps increasing transmission window, it eventually becomes too big for network. Congestion detection reduces the rate back.
 TCP assumes that if RTO has triggered or 3 dup ACKs have been received (segment lost), network is overloaded.
 If timeout occurred, TCP has strong suspicion of congestion so -
  1. sets ssthresh = cwnd/2
  1. set cwnd = 1 and restart slow start phase.
 If 3 dups were received, TCP has minor suspicion of congestion so -
  1. sets ssthresh = cwnd/2
  1. cwnd = ssthresh and start congestion avoidance phase (aka fast recovery)


# Fast retransmit
 When dup ACK is rcvd, TCP does not know if segment was lost or delayed hence
 rcvd out of order. In out of order condition, usually 1 or 2 dup ACKs are rcvd
 before rcvr reorders the segments. If more than 2 dup ACKs are rcvd, it is
 likely that a segment has been lost. So, when 3 or more dup ACKs are rcvd,
 sender does not even wait for retranmission to expire before resending the
 segment. This is fast retransmit.

# Fast recovery
 Since dup ACKs can only be generated when data is rcvd, when sender gets more
 than 3 dup ACKs it knows data is still flowing to rcvr. So instead of reducing
 data flow to slow start (1 segment), sender enters congestion avoidance mode.

# TCP timers

## Retransmission timer
This timer handles the RTO. But to calculate RTO, first RTT needs to be calculated.
Initially 1.5 sec. Set timer when segment is sent. Cancel the timer when ACK is received. If timer goes off, retransmit the segment.
Timer has to be just right, so use adaptive or smoothed timeout to adjust timeout dynamically.
To calculate smoothed timeout, keep estimate of average RTT and it's variance.
```
SRTT(n) = 0.9 * SRTT(n-1) + 0.1 * RTT(n)
Svar(n) = 0.9 * Svar(n-1) + 0.1 * [RTT(n) - SRTT(n)]
timeout = SRTT(n) + 4 * Svar(n)
```

## Persistence timer
If receiver advertizes 0-window, sender stops until receiver sends an ACK. In case, this ACK gets lost, both sender and receiver will sit waiting for each other.
So when 0 rwnd is advertized, sender starts a persistence timer (initially equal to RTO).
If this timer expires, sender sends a probe segment, which is only 1byte.
If received, client can ignore it and send ACK again. But if sender still doesn't hear back, it'll send another probe after double the wait.
After timer value reaches 60s, sender sends one probe every 60s.

## Keepalive timer
For long-idle connections. Each time sender hears from receiver, it resets this timer (usually 2 hour).
After this timer expires, sender sends a probe segment. After 10 such probes, server can drop the connection.

## TIME-WAIT timer
Used during connection termination.


# TCP options
TCP header has upto 40byte of options.
  1. EOP - 1 byte signifying end of options
  1. NOP - padding byte
  1. MSS - maximum size of data (not segment) [kind:1byte, length: 1byte, size: 2bytes]
  1. Window Scale Factor - used to increase window size
  1. timestamp
  1. PAWS - protection against wrapped sequence numbers.
  1. SACK permitted and SACK options

