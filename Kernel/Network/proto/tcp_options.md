
# TCP_CORK
Nagle found that with very small packets, network utilization was very low.
His solution was to wait for ack from peer before sending next packet. This way kernel gets time to coalesce multiple packets in to one.
Nagle's algorithm is default behavior for TCP in Linux.

However, this also increases latency. To get around this delay, `TCP_CORK` option can be used with TCP to signal kernel to send all pending data.
Once socket is created, using `setsockopt()` set this flag. Once data is ready to be sent, remove the flag.
```c
int state = 1;
setsockopt(fd, IPPROTO_TCP, TCP_CORK, &state, sizeof(state)); // set TCP_CORK
state = 0;
setsockopt(fd, IPPROTO_TCP, TCP_CORK, &state, sizeof(state)); // unset 
```
There is a 200ms ceiling on corking the write. Once passed, data will be automatically sent. This option is specific to Linux only.

# TCP_NODELAY
If you have multiple buffers to write, instead of using `TCP_CORK` and wait to write all, use gather function `writev()`.
Then either `TCP_CORK` can be used or `TCP_NODELAY`. Latter effectively turns Nagle's algorithm off and sends packet immediately (flush).

Another alternative is to write all data to a temporary buffer and then use `write()` to send, while `TCP_NODELAY` is set.
This obviously requires extra copy of data.

# TCP_INFO 
Get `struct tcp_info` which provides information on this socket.

# Related to SO_KEEPALIVE
If the socket option `SO_KEEPALIVE` has been set on this socket -

## TCP_KEEPIDLE
The time (in seconds) the connection needs to remain idle before TCP starts sending keepalive probes.

## TCP_KEEPINTVL
The time (in seconds) between individual keepalive probes.

## TCP_KEEPCNT
The maximum number of keepalive probes TCP should send before dropping the connection.

https://linux.die.net/man/7/tcp
