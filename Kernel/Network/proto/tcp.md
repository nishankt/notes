
TCP relies on endpoints (host, port) to differentiate applications, where host is IP addr.
A TCP connection is defined by two endpoints.

# Message format
```
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |          Source Port          |       Destination Port        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                        Sequence Number                        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Acknowledgment Number                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Data |           |U|A|P|R|S|F|                               |
   | Offset| Reserved  |R|C|S|S|Y|I|    Window size (bytes)        |
   |       |           |G|K|H|T|N|N|                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |           Checksum            |         Urgent Pointer        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Options                    |    Padding    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                             data                              |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```
Sequence number identifies position in sender's octet stream
Ack number identifies number of octet source expects to receive next.
Data offset is essentially header length, after which data starts. It's needed due to variable options field.
6 bits after data offset denote purpose and content of the segments
  URG - If set, "Urgent Pointer" is valid. Data is added in the front of segment. "Urgent Pointer" points to end of urgent data. Note, this is not priority data, how it gets interpreted is app specific.
  PSH - like buffer flush. Dont wait for buffer to fill and push the segment
  ACK/SYN/FIN/RST
Window field identifies how much data it is willing to accept (buffer size)
TCP options -
  Maximum Segment Size (MSS) - option to allow receiver to specify maximum sized segment it is willing to receive.
  Window Scaling Option - mainly for large delay-bandwidth product like Satellites, where 64K window size is not enough.
  Timestamp Option - used to compute delay in network.

One goal of TCP is to match transmission rate of sender to that of receiver and the network.


# TCP reliability
 * detect missing, reordered, dup data (through ack & seq number)
 * detect corrupted data (through checksum)
 * prevent overrunning receiver (through flow control)
 * error recovery (through retransmit)

TCP's 16 bit 'window' field is used by rcvr to tell the sender how many bytes
of data the receiver is willing to accept. So max is 64K. However, RFC1323
allows windows to increase from 16 to 32 bits, hence much more data.

# Connection
## Establish connection
TCP uses 3-way handshake - SYN SYNC/ACK ACK

## Closing connection
Can be 3-way handshake (FIN, FIN/ACK, ACK).
Or, half-close which is one end can send FIN and stop sending data. Other end can send ACK, as well do similar half-close.

## Reset
Deny, terminate or abort existing connection via RST.


# Sliding window
A simple [send+ack] or send-and-wait algorithm is not efficient. It allows only for 1 segment in flight.
Instead if multiple segments are sent, bandwidth can be better utilized.
TCP keeps a window of buffer, on which it is currently working.
```
              LAR                       R
                v                       v
 [.... ack'd ... | sent      : not sent | not processed  .....]
                 ...W........^...........
                            LFS
LAR = last ack received
LFS = last frame sent
LFS - LAR <= W
```
## Go-back-N
Simple sliding window algorithm but inefficient
Receiver keeps only single packet buffer for next segment.
```
LAS = last ack sent
On receive
  if seq number is LAS+1
    accept and pass it up to app
    update LAS and send ACK
  else discard
```
## Selective repeat
More complex, but better performance. Used by TCP
Receiver keeps a bigger buffer. It acks highest in-order segment along with hints about out-of-order segments.
```
LAS = last ack sent
On receive
  buffer segments [LAS+1, LAS+W]
  accept and pass in-order segment from LAS+1 onwards
  update LAS
  send ACK for LAS
```

TCP by default uses Cumulative ACK, which tells next expected byte sequence number (LAS+1).
Optionally, Selective ACK (SACK) can send upto 3 ranges of received bytes, so hints missing segments.


# TCP algorithms

## TCP Tahoe
Included slow start, congestion avoidance and fast recovery.

## TCP Reno
Included Tahoe implementations plus fast retransmit.

## TCP NewReno 
Applies only if SACK has not been negotiated.

# Other optmizations
Initial wnd size: increased from 1 segment to 4K.
TCP Pacing: 


# Debugging TCP
Via wireshark, focus on following -
  1. TCP retranmissions
  1. TCP out-of-orders
  1. Dup ACKs
  1. Zero windows
