# IPv4 addressing
32bit or 4 octets

## Classful
Only 4 classes of networks were allowed, which determined how many subnets.
 addr = network + subnet + host
 subnettting - fixed length subnets, variable length subnets
 Represent subnets with mask, slash notation

## Classless (Classless Inter-Domain Routing)
Allows subnet of any size (2^n) from any available IP range.
 subnets with slash notation
 private subnets
       10.0.0.0/8
    172.16.0.0/12
   192.168.0.0/16
   169.254.0.0/16 [used for autoconfigure]

# IPv6
128bits or 16 octets or colon hex (8 16bit hex)
Zero compression - can be applied only once to consecutive 0 16bit hexes (ff05::b3)
  can also be used to represent IPv4 - ::128.0.0.1

       |------ N bits -----|   |- 64-n -|  |- 64 bits -|
addr = global routing prefix + subnet id + interface id


# Header and datagram format
## IPv4
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |Version|  IHL  |Type of Service|          Total Length         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |         Identification        |Flags|      Fragment Offset    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Time to Live |    Protocol   |         Header Checksum       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                       Source Address                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Destination Address                        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Options                    |    Padding    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Payload                                    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

## IPv6
   | Base header | Extension header 1 | ... | Extension header N | Payload |
Each header area has NEXT field which points to next header or eventually payload.

In base header (following), "Next header" specifies type of next header and not pointer.
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |Version| Traffic class |          Flow label                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |        Payload length         | Next header   | Hop limit     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                       Source Address                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Destination Address                        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

The extension headers are of type - hop-by-hop, routing, fragment, authentication, etc.
Each header is of different format and size but usually has `| next header | header len | options |`

# Fragmentation
MTU (Maximum transfer unit) is the size of frame allowed by L1. Ethernet allows MTU of 1500 bytes.

With IPv4, router can fragment bigger sized MTU to smaller in order to accomodate it in network path.
The fragmentation is delayed and must be performed only when necessary.
To fragment a datagram (say of size 1400) for MTU of 600, fields `Flag` and `Fragment offset` are used in the IPv4 header. The first fragment has "more fragments" bit set in `Flag` and `offset` fields contains offset for start of data in the original datagram. So 1400 sized datagram becomes -
  `[F + 0 + {600byte payload}], [F + 600 + {600b payload}], [_ + 1200 + {200b payload}]`
As per RFC, every node should be able to foward a datagram of 68bytes for IPv4 (1280 for IPv6) without fragmentation.
Minimum reassembly buffer is 576bytes for IPv4 (1500 for IPv6)

IPv6 does not allow routers to fragment, so minimum common MTU must be learnt by original source.
This was since IPv6 committee thought ATM mode would become prevalent where sender pre-establishes path to destination.
PMTUD (Path MTU Discovery) sends IPv6 datagram to consisting to path fitting MTU to next hop. If next path has smaller MTU, it sends back an ICMP error message to notify original source of smaller MTU.
What is route changes and overall MTU increases? IPv6 governs that source periodically sends PMTUD datagram to discover bigger sized MTU path. Usually, 10min is used before trying again.
When fragmentation is needed at source, IPv6 inserts fragment extension header which includes offset field.

