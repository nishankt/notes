
# HyperText Makrup Language
HTML documents contains text along with embedded commands (tags) that provide guidance on rendering the text.

# Uniform Resource Locators
Each HTML document is assigned a unique name that identifies it.
`http:://hostname[:port]/path[;parameters][?query]`

# HyperText Transfer Protocol
The protocol used to transfer html files between client/browser and web server.
HTTP is stateless, i.e. saves no history after request/response query.

## GET
Request to obtain HTML for provided URL. `GET address HTTP/version`
With HTTP1.1, once TCP connection has been established, it stays open (persistent connection) until explicitly closed by either end.
To allow TCP to persist connection, server sends the length before each response.
Each HTTP tranmission contains header of format `keyword: info`. Keyword can be "Content-length", "content-type", "content-language", etc.
If server does not know the length, server closes the connection after sending response. However, server does send "Connection: close" header as warning to client.

## Error messages
If server has to respond with error message, it does so by embedding it in HTML page itself.
It uses `<HEAD>` tag for error code and message, while `<BODY>` for details of error.

## Negotiation
HTTP headers can also be used to negotiate capabilities (authenticated, allowed type of media, compression ok, content details, page validity, etc).
Server-driven negotiation begins with browser/client which specific list of preferences. Server tries to satisfy preferences based on local policy.
Agent-driver negotiation is two-step process. Firstly, browser asks what capabilities are supported by server. Server responds and then client sends request based on available capabilities.
"Accept" header is used to specify capabilities.
`Accept: text/html, text/plain; q=0.5, text/x-dvi; q=0.8` where `q` is preference level.

## Conditional request
Sender can send conditional header which server can honor. "If-Modified-Since" is most common.
`If-Modified-Since: Mon, 01 Apr 2019 05:00:00 GMT`
