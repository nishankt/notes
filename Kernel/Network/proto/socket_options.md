

# SO_KEEPALIVE
Keep sending keepalive messages on connection-oriented sockets.

# SO_LINGER
`close()` and `shutdown()` return immediately. But if `SO_LINGER` is set then they will not return until all messages are flushed or linger timeout has been reached.
The socket always lingers in background if it is closed via `exit()`.

# SO_REUSEADDR
https://stackoverflow.com/a/14388707
Since transport identifies each connection via a tuple (proto, src addr, src port, dst addr, dst port), this tuple should be unique for each connection.
On server side, one of src_addr:src_port should vary per connection too. (e.g. 1.1.1.1:21 and 1.1.1.2:21 are OK but 1.1.1.1:21 again would throw EADDRINUSE)
If server side chooses ANY_PORT, then _one_ of available port is picked when connection is made.
If server side chooses ANY_ADDR, then it binds to _all_ local addresses.
Hence, if ANY_ADDR was set as option, then no other address for same port can be used.
However, if `SO_REUSEADDR` was set then as long as address is not _exactly_ same, same port can be used for another address.
Also, due to TIMED_WAIT after `exit()` or if `SO_LINGER` option was set, the address will be considered bound even though process has exited. With `SO_REUSEADDR` option set, socket can re-bind to address/port which is still in `TIMED_WAIT`.

# SO_REUSEPORT
This option allows an arbitrary number of sockets to bind to same port, as long as all the prior sockets on same port also used `SO_REUSEPORT`.
To prevent port hijacking, Linux dictates that all sockets sharing same address/port combination must belong to same user ID.

If an address/port is already in use, then using `bind()` on it throws EADDRINUSE error. But `connect()`, which is on client side can also throw EADDRINUSE!
This would happen is client used same address/port to connect to server had either `SO_REUSEADDR` or `SO_REUSEPORT` option set, tuple for 2 connections would be same.


https://linux.die.net/man/7/socket
