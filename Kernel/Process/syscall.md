
| syscall method | notes                                              |
|----------------|----------------------------------------------------|
| int 0x80       | legacy way; avoided call gates                     |
| SYSENTER       | instruction used on 32-bit to invoke system call   |
| SYSCALL        | default way on x86_64; unavailable on 32-bit       |

# Legacy
Given user can generate interrupt by using `int` assembly instruction and on to handle interrupt kernel will run, syscall method #1 can be created.
Linux kernel registers an interrupt handler for interrupt number `0x80` via `ia32_syscall()`. The entry point in kernel is -
```c
void __init trap_init(void) {
  ...
  // IA32_SYSCALL_VECTOR = 0x80
  set_system_intr_gate(IA32_SYSCALL_VECTOR, ia32_syscall);
}
```
Userspace process will copy desired system call number to `%eax` and trigger `int 0x80`. Arguments are in register `ebx, ecx, edx`, etc.
CPU finds kernel entry address by looking up IDT (intr descriptor table).
IDT will be trap gate to kernel mode and contains entry for ISR to run, which calls appropriate system call handler.
`{push regs to stack; kernel code; pop regs; iret;} `
Above also causes kernel stack to be switched.
TSS (Task state segment) provides kernel stack to push return address.
All data struct saved in stack is Interrupt frame.
Once syscall is done, `iretq` passes the control back to user-space.

# Fast system calls
Interrupts are slow and have significant overhead. Fast system calls are modern way to call interrupts.

## sysenter / sysexit (32-bit)
For `systenter`, kernel must set 3 MSRs - IA32_SYSENTER_EIP, IA32_SYSENTER_CS, IA32_SYSENTER_ESP. Through the IA32_SYSENTER_EIP MSR, address of function to execute on syscall is passed (`ia32_sysenter_target`). Kernel sets up this MSR by writing address of `ia32_sysenter_target()` into it.

Again, `eax` should have system call number and arguments in other registers.

With interrupts, registers were saved on stack and later popped into respective registers via `iretq`. But `sysenter` does not store return address, so kernel does some bookkeeping prior to syscall. A wrapper for bookkeeping logic is provided in kernel `__kernel_vsyscall()`, but it also mapped into each userspace as well. It is part of vDSO. The address of `__kernel_vsyscall()` is written into ELF auxillary vector, where a program or glibc can find it. It can be found by `getauxval` with `AT_SYSINFO` argument, or by iterating to the end of env variables. The bookkeeping is saving various registers onto stack.

So, instead of calling `sysenter` directly, steps are -
 1. search ELF aux vector for `__kernel_vsyscall()` address
 1. put system call number and arguments in registers
 1. call `__kernel_vsyscall()` (which sets up stack and calls sysenter)

A bit about auxillary vector's location. It is as follows.
```
  +----------------+
  |auxillary vector|
  |environ         |
  |argv            |
  |stack           |
  vvvvvvvvvvvvvvvvvv
```

The entry point in kernel is -
```c
systern_dispatch:
  call *ia32_sys_call_table(,%rax,8)
```

Once call is complete, `sysexit` instruction resume execution back to user program. But before that caller to `sysexit` must put return address and pointer to program stack in rdx and rcx registers. This is done by kernel code `sysexit_from_sys_call()`, which eventually calls `sysexit`.

## syscall / sysret (64-bit)
Similar to sysenter but MSR used is IA32_LSTAR for writing address of function os entry. Kernel sets this MSR by writing `system_call()`'s address into. Like other methods, userspace needs to follow convention by putting system call number in rax and arguments in other regsiters. `syscall` can be directly called by the program after that. The `system_call()` function saves return address in %rcx, which is used while returning via `sysret`.

# glibc's syscall wrapper
Instead of writing inline assembly, better method is to use `syscall()` from glibc.
```c
unsigned long syscall_nr = 60;
long exit_status = 42;
syscall(syscall_nr, exit_status);
```
Internally, glibc just maps calling convention from userspace to kernel's ABI. It moves registers accordingly and eventually calls `syscall`.

# Virtual system calls (vDSO)
This is the way to do syscall without entering kernel at all! vDSO (virtual dynamic shared object) is set of code from kernel, which is mapped to user space program. Very frequent and lightweight system calls like `gettimeofday` uses this method.

Since vDSO could be mapped anywhere in userspace address, just like `__kernel_vsyscall()`'s addres, it is located by searching ELF auxillary headers to find a header of type AT_SYSINFO_EHDR. In both these cases, kernel writes address to ELF header when program was loaded.

# Call gates
SYSCALL/SYSENTER instruction is executed through VDSO, virtually mapped to
each running process. To call a routine in VDSO, following code is used -
`call *%gs:0x10`
which contains all the code supporting SYSENTER/SYSCALL instruction.

At boot time a page is dedicated to containing subset of syscalls, deemed safe
to execute from userspace.

e.g. in gettimeofday, a special variable is mapped into memory location where
kernel updates it and userspace can read it. glibc internally knows how to 
access this location and provides the data.
```
arch/x86/vdso/vgetcpu.c
arch/x86/vdso/vdso.lds.S
```

# linux-gate.so.1
Earlier library was called linux-gate.

# References
https://blog.packagecloud.io/eng/2016/04/05/the-definitive-guide-to-linux-system-calls/
https://x86.lol/generic/2019/07/04/kernel-entry.html
https://lwn.net/Articles/604287/
https://lwn.net/Articles/604515/
http://www.linuxjournal.com/content/creating-vdso-colonels-other-chicken?page=0,0
https://www.ibm.com/developerworks/linux/library/l-system-calls/

