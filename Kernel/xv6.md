# JOS bootup
## Initial intructions
```
0xffff0:  ljmp $0xf000, $0xe05b   
```
After reset, processor enters real mode and set CS to 0xf000 and IP to 0xfff0. So the execution begins at CS:IP segment address. QEMU has already placed SeaBIOS in hardcoded BIOS ROM area (0xF0000 to 0x100000). CS:IP (as per real mode addressing -> 16 * segment + IP = 0xFFFF0) is 16bytes below 1MB (0xFFFF0). But since BIOS cant do much in 16b, so BIOS jumps to 0xFE05B as first instruction, something specific to SeaBIOS.
```
0xfe05b:  cmpl $0x0, $cs:0x6ac8
```
SeaBIOS is comparing some POST flag to determine if it's reboot or resume. In [SeaBIOS](https://github.com/coreboot/seabios/blob/master/src/romlayout.S), it is variable `HaveRunPost`. If it's `0` then its a reboot else resume. But this area is "ROM", so then how is SeaBIOS changing it? SeaBIOS temporarily makes the shadowed memory from 0xf0000 to 0x100000 writeable by changing the PAM settings through PCI configuration space (on intel) which allows modification of the `HaveRunPost` memory location. SeaBIOS's `make_bios_writable` function is used for that purpose as part of its shadow memory functionality.
```
0xfe062:  jne  0xfd2e1
```
Again from SeaBIOS code `jnz entry_resume`.
```
0xfe066:  xor  %dx, %dx
0xfe068:  mov  %dx, %ss
0xfe06a:  mov  $0x7000, %esp
```
Above clears stack segment register, and set ESP to stack top (0x7000). Comes from [SeaBIOS](https://github.com/coreboot/seabios/blob/master/src/entryfuncs.S#L154)
```
0xfe070:  mov  $0xf34d2, %edx
0xfe076:  jmp  0xfd15c
0xfd15c:  mov  %eax, %ecx
0xfd15f:  cli
0xfd160:  cld
```
Save some pointer to EDX which will be used later.
Reset interrupt and direction flags. From here, the code is [here](https://github.com/coreboot/seabios/blob/master/src/romlayout.S#L360).
```
0xfd161:  mov  $0x8f, %eax
0xfd167:  out  %al, $0x70
```
Ports 0x70 and 0x71 are control registers for [CMOS](http://bochs.sourceforge.net/techspec/PORTS.LST).
Disable NMI through 70H I/O port by writing 0x8f to it. Select register 0xF from CMOS with NMI disabled. The lower order 7 bits are used to select register, and the most significant bit determines whether to disable NMI. Register 0xF is CMOS Shutdown Status.
```
0xfd169:  in  $0x71, %al
```
Read CMOS shutdown status.
```
0xfd16b:  in  $0x92, %al
0xfd16d:  or  $0x2, %al
0xfd16f:  out  %al, $0x92
```
Enable A20 address line, which is needed for protected mode. Used to control memory 1MB barrier. Value read from port 0x92 (PS/2 system control port) is `or`d with 0x10 to set bit 1, indicating A20 is ennabled.
```
0xfd171:  lidtw  %cs:0x6ab8
0xfd177:  lgdtw  %cs:0x6a74
```
Load interrupt and global descriptor tables from some particular address. lgdt will load 6bytes starting from %cs:0x6a74 to GDTR.
```
0xfd17d:  mov  %cr0, %eax
0xfd180:  or  $0x1, %eax
0xfd184:  mov  %eax, %cr0
```
CR0 is a 32-bit control register, whose first bit (bit 0) is the Protection Enable bit. Using `or 0x1`, enable protected mode.
```
0xfd187:  ljmpl  $0x8, $0xfd18f
0xfd18f:  mov  $0x10, %eax
0xfd194:  mov  %eax, %ds
0xfd196:  mov  %eax, %es
0xfd198:  mov  %eax, %ss
0xfd19a:  mov  %eax, %fs
0xfd19c:  mov  %eax, %gs
```
0x8 and 0x10 are segment selectors, which are basically the indices of entries in the GDT. After loading GDTR, it's necessary to reload all the segment register.
```
0xfd1a0:  jmp *%edx
```
An indirect jump for value we moved to EDX in the beginning.
```
0xf34c2:  push %ebx
0xf34c3:  sub $0x2c,%esp
```
Setting up stack frame according to x86 calling convention.
```
0xf34c6:  movl $0xf5b5c,0x4(%esp)
0xf34ce:  movl $0xf447b,(%esp)
0xf34d5:  call 0xf099e
```
A=0xf5b6c and B=0xf430b.
```
0xf099e:  lea 0x8(%esp),%ecx
0xf09a2:  mov 0x4(%esp),%edx
0xf09a6:  mov $0xf5b58,%eax
```
Loading address of A into ecx, and value of B into edx.
From here on it will now initialize PCI devices and continue until bootloader takes over.

