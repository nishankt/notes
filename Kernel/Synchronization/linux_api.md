1.  Atomic operations
1.  Spinlocks
1.  Semaphores
1.  Mutexes
1.  Completion variables
1.  Sequential locks (seqlock)
1.  RCU locks
1.  Preemption disabling
1.  Ordering and barriers
1.  pthreads (userspace)
1.  futex (userspace)

# Atomic operations
Operations on integers, which operate on datatype atomic_t {contains 'volatile int'} or `atomic64_t` {contains 'volatile long', i.e. 64bit}
```c
  atomic_t v;                   // define only
  atomic_t v = ATOMIC_INIT(0);  // define and init
  atomic_set(&v,4);
  atomic_add(2, &v);
  atomic_inc(&v);
  atomic_read(&v);              // convert atomic_t to int
  atomic_inc_and_test(&v);      // atomically dev and test if zero
  ATOMIC64_INIT(long i);        // define and init 64 bit atomic
```
Bitwise operations include -
```c
  set_bit(0, &word);            // set bit 0 atomically
  clear_bit(0, &word);
  change_bit(0, &word);
  test_and_set_bit(0, &word);
```
Non-atomic version of above APIs are `__func()`.

# Spinlocks
Locks provide concurrency in multiprocessor system. On uniprocessor, locks simply enable or disable preemption. If preemption is disabled, locks are no-op. Spinlocks on uniprocessor make no sense, so it only disables preemption i.e. disables interrupt.
Linux kernel (unlike few other OS) does not supports recursive spinlocks.
```c
  DEFINE_SPINLOCK(mr_lock);
  spin_lock(&mr_lock)/spin_unlock(&mr_lock);
```
Spinlocks can be used in interrupt handlers (unlike semaphores), but one must disable further interrupts.

`spin_lock_irqsave(&mr_lock, flags)/spin_unlock_irqrestore(&mr_lock, flags);`
On uniprocessor, above only disables/enables interrupt along with enabling/disabling preemption. On MP, disables/restores IRQ on local CPU.
`spin_lock_bh()` disable software interrupts but not hardware.

If sure that interrupts are enabled prior to critical section, `spin_lock_irq(&mr_lock)/spin_unlock_irq(&mr_lock)` will disable and enable interrupts unconditionally. Not recommended!
```c
  spin_lock             IFF spinlock wont be used in interrupt handlers
  spin_lock_irqsave     disables local interrupts and does global lock
  spin_lock_irq         IFF known that interrupts are enabled already (efficient)
  spin_lock_bh          disables s/w interrupts but leaves h/w intr enabled
```

`spin_lock_init()` to initialize a dynamically created spinlock.
`spin_trylock()` attempts to get spinlock. If fails, instead of spinning returns 0 else obtains the lock and returns non-zero.
`spin_is_locked()` returns non-zero if lock is already taken, else zero. But never obtains the lock itself.

Uses ticket spinlock (implemented via fetch-and-increment). Plain spinlock have no concept of fairness, i.e. it doesn't favor thread waiting longest. With ticket spinlock, an ordering is imposed among threads trying to access critical section.

# Reader-writer spinlock
One or more readers can concurrently hold reader lock but writer lock only by one.
```c
  DEFINE_RWLOCK(mr_rwlock);
  read_lock(&mr_rwlock) / read_unlock(&mr_rwlock);
  write_lock(&mr_rwlock) / write_unlock(&mr_rwlock);
  read_lock_irq / write_lock_irq
  read_lock_irqsave / write_lock_irqsave
  write_trylock
  rwlock_init       //for dynmically creating rwlock_t
```
In kernel, reader locks are favored over writer locks. Hence writer will get lock only when all pending readers have acquired and released locks.

# Semaphores
These are sleeping locks. If lock is unavailable, process is put on waitqueue and sleeps. Only acquired in process context and never in interrupt context as semaphores sleep. Semaphores obviously do not disable preemption.
Binary or counting semaphores. Binary sem is mutex (i.e. only one allowed to access) while counting semaphore is more about limiting users to access something (not mutually exclusive anymore as multiple users have access).
```c
   struct semaphore name;
   sema_init(&name, count);      //for counting semaphore
   static DECLARE_MUTEX(name);   //for binary semaphore
   sema_init(&sem, count) ;      //for dynamically creating counting semaphore
   init_MUTEX(&sem);             //for dynamically creating binary semaphore
   init_MUTEX_LOCKED(&sem)       //for dynamically creating binary semaphore
                                 // already locked (count of 0)
   down_interruptible(&mr_sem);  //try to acquire sem, if not then put task in
                                 //   TASK_INTERRUPTIBLE
   down(&mr_sem);                //try to acquire sem, if not put in
                                 //   TASK_UNINTERRUPTIBLE
   down_trylock()                //try to acquire lock with blocking. If lock held
                                 //   already, return with nonzero, else grab lock
                                 //   and return 0
   up(&sem)                      //release a lock
```

# Reader-write semaphores
All reader writer semaphores are mutexes (i.e. usage count is 1) but only enforce exclusion on writers not on readers. They are use uninterruptible sleep.
```c
   static DECLARE_RWSEM(name);
   init_rwsem(&sem);
   down_read(&mr_sem)/up_read(&mr_sem);
   down_write(&mr_sem)/up_write(&mr_sem);
   down_read_trylock(&sem);
   down_write_trylock(&sem);
   downgrade_write(); atomically converts an acquired write lock to read lock
```

# Mutex
Mutual exclusion through semaphore has overhead, hence simplified version of semaphore (0 or 1) via 'mutex'. A process cannot exit while holding a mutex. Cannot be acquired by interrupt handler or by bottom halves. Should be locked and unlocked by same entity.
```c
   struct mutex;
   DEFINE_MUTEX(name);      //statically define a mutex
   mutex_init(&mutex);      //dynamically initialize a mutex
   mutex_lock(&mutex) / mutex_unlock(&mutex);
   mutex_trylock(&mutex)
   mutex_is_locked(&mutex)
```
Mutex and binary semaphore have technical difference - mutex is locked and unlocked by same resource. So difference is about principle of ownership.

# Completion variables
Way to synchronize between tasks within kernel. One task wait on completion variable while other performs work. When other task is done, it uses completion var to wake waiting tasks. Again, same as semaphores but much simpler.
```c
  struct completion;
  DECLARE_COMPLETION(mr_comp);  //statically created and intilized
  init_completion(&mr_comp);    //dynamically created
  wait_for_completion(&mr_comp) //wait for given completion var to be signaled
  complete(&mr_comp)            //singal any waiting tasks to wake up e.g. parent process waiting for child to finish
```

# Sequential locks
A simple mechanism for reading/writing shared data. It maintains a sequence counter. Whenever shared data is written, lock is obtained and sequence counter incremented. Before and after reading the data, seq num is read. If values are different, then data was written to. Good for lightweight, scalable, many reader and few writer scenario.
```c
  seqlock_t mr_slock = DEFINE_SEQLOCK(mr_slock);
  write_seqlock(&mr_slock)/write_sequnlock(&mr_slock)
  do {
      seq = read_seqbegin(&mr_slock);
      /* read data */
  } while (read_seqretry(&mr_slock, seq);
  e.g. get_jiffies, set_jiffies
```

# Read-copy update
The idea is to split updates into 'removal' and 'reclamation' phase. When the writer wants to update, it. One main thing  is read and write cycle can happen concurrently.
  a. removes reference to the data struct, so that readers cannot gain ref to it
  b. waits for all previous reader to complete their RCU reads
  c. safely reclaim (free) older reference
```c
  //Reader side:
    rcu_read_lock
    rcu_dereference              //read the value
    rcu_read_unlock
  //Updater side:
   kmalloc(new)
   spin_lock                     //to avoid concurrent updates
   new = old
   rcu_assign_pointer
   spin_unlock
   synchronize_rcu/call_rcu      //make sure all readers that have reference are
                                 // done. If dont want to block, then use
                                 // call_rcu, which invokes a function
   kfree(old)
```

# Preemption
If spinlock not needed but still want preemption (e.g. in per-processor data). If data is unique to processor, no need to take a lock as only one processor can access it. Preemption count stores the number of held locks. If number is zero, kernel is preemptible.
```c
  preempt_disable() / preempt_enable()
  preempt_enable_no_resched();  //enable kernel preemption but not check for any pending rescheds
  preempt_count()               //return preemption count
```
  A better way to access per-CPU data is by following =
```c
  int cpu;
  cpu = get_cpu();  //disable preemtion and set cpu to current CPU
  put_cpu();        //enable preemption
```

# Ordering and barriers
Both compiler and processor can reorder or optimize loads/stores
```c
   rmb()        //read memory barrier (ensures no loads are reordered across rmb)
   wmb()        //write barrier (ensures no stores are reordered across wmb)
   mb()         //both read and write barrier
   read_barrier_depends()   //read barrier only loads on which subsequent load depends
   barrier()    //prevents compiler from optimizing load or store acorss call. However, CPU can still reorder though.
```
smp_rmb, smp_wmb, smp_mb, smp_read_barrier_depends are regular memory barriers in SMP but only compiler barriers on UP. (so on UP they are just barrier())
x86 does not do out of order stores, hence wmb() does nothing. Also on x86, following are 'serializing' hence act as a memeory barrier -
  - all instructions that operate on I/O ports
  - all instructions prefixed by 'lock' (atomic ops)
  - all registers that write into control regs, system regs, or debug regs
  - lfence, sfence, mfence which are effcient memory barriers
  - iret, which terminates interrupt/exception handler

# Pthreads
Part of linux's NPTL. It is 1:1 model.
APIs are 
```
 pthread_             for controlling threads and miscll
 pthread_attr_        for controlling thread attribute objects
 pthread_mutex_
 pthread_mutexattr_
 pthread_cond_
 pthread_condattr_
 pthread_rwlock_
 pthread_barrier_
 pthread_key_         for thread specific data keys
```

# Futex
Fast userspace mutex is a kernel system call that programmers can use to implement locking or building blocks for higher level synchronization abstractions. In linux, system call provides way to program to wait for value at given address to change, or wake up anyone waiting on a given address.
```c
int futex(int *uaddr, int op, int val, const struct timespec *timeout, int *uaddr2, int val3);
```
where op can be FUTEX_WAIT, FUTEX_WAKE, FUTEX_FD, FUTEX_REQUEUE, FUTEX_CMP_REQUEUE
