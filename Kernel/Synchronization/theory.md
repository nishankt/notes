# x86 
- Assembly language instructions that make 0 or 1 aligned memory access are 
  aligned
- read-modify-write cycle is not atomic (inc, dec)
- r-m-w instructions prefix'd by 'lock' are atomic, i.e. memory bus is locked
- instructions prefix'd by 'rep' force control unit to repeat same instruction
  several times. They are not atomic though, control unit checks for pending
  interrupts (bus level) before executing new instruction.

# Preemption
While kernel is executing on behalf of user process, can be preempted
and start working on behalf of some other user process. E.g. while
kernel executing in process A context, interrupt occurs which awakens
high priority user process B. On return from handler, kernel will be
executing on behalf of process B. Later when process A is scheduled, 
kernel resumes A's things.

Interrupt handlers, softirqs, and tasklets are all non-preemptable and
 non blocking. Although there can be nested interrupts. 
Softirqs and tasklets cannot be interleaved on a given CPU
Same tasklet cannot be executed simultaneously on multiple CPUs
Hence, interrupt handlers and tasklets need not be coded as reentrant functions
Per-CPU vars accessed by softirqs and tasklets only do not require sync
A data struct accessed by only one kind of tasklet does not need sync

# spinlock vs mutex
- In theory, if cannot acquire lock then spinlock spins and mutex yields.
- So on uniprocessor, spinlock is useless (hence implemented as interrupts disable) or will outright cause deadlock. Mutex is more useful by putting the thread in sleep.
- Mutex, however, still have significant overhead (due to context swtich). But on multi-processors there are cases where spinlock is better (like when only few CPU cycles of critical section).
- But developers dont know in advance if mutex will be useful or spinlock. So, many OS will choose mutex or provide hybrid approach.
- Hybrid approach does inital few spins as spinlock and if lock still not available then puts thread to sleep.

# Semaphore vs mutex
- While mutex is exclusively locking mechanism, semaphmores can be used as both locking (binary semaphore) and signalling mechanism.
- Mutex are locked and unlocked by same process/thread but semaphores can be changed by any thread/process.
- Semaphores release lock and get put on waiting queue (sleep), while mutex always hold the lock while they are scheduled out (but still runnable)
