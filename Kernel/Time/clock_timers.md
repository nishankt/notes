https://stackoverflow.com/questions/22437491/how-linuxs-alarm-is-handled-by-kernel?answertab=votes#tab-top
https://venam.nixers.net/blog/unix/2020/05/02/time-on-unix.html

# Locale
All date commands rely on locale which is internationalization mechanism.
Some useful utility and files
```
$ locale
$ cat /usr/share/zoninfo
$ cat /etc/timezone
$ echo $TZ
```

# UTC / Epoch time
Unix Time (UTC) - 1st of January 1970 00:00:00 UTC
Epoch time - 1st of January 1971
32 bit signed integer. However, that would last only until 2038. So now 64-bit UTC being used.

# Clocks
## System clock/Kernel clock/SW clock
Track time using counter since epoch. Runs only when system is on.
## RTC/CMOS clock/HW clock
Varies in implementation across architecture - 2Hz and 8192Hz, from 0.5s to 0.1ms precision.
There could be multiple HW clocks in same system.
Linux maps RTC clocks to `/dev/rtc*`
System clock is intialized at boot by sync'ing to RTC (via `hwclock` utility). Later, NTP can be used.
`date --set` can be used to set system clock as well.
RTC is preffered to be in UTC, while system clocks should set as per timezone.
## TSC (time-stamp counter)
`rdtsc` instruction to read.
TSC is a per cpu 64-bit register on x86. It is driven by CLK input pin, hence same frequency as CPUs.
But since CPU frequency are variable with c-states, TSC should change. However, with Intel invariant TSC, it stays same across c-states. cpuid 800000007, edx: 8th bit is for TSC invariant.
kernel also enables nonstop_tsc if this is enabled (early_init_intel) - `set_cpu_cap(X86_FEATURE_NONSTOP_TSC)`
## HPET (high precision event timer)
HPET is a separate chip which can provide time.
While it is high precision it has sync overhead involved and more so with multi socket. In kernel, TSC is preffered over HPET. 
## RTC vs TSC vs HPET
Reading RTC is costly, but TSC takes 1 instruction.
RTC is at best 1/18 of seconds, while TSC can be in nanoseconds
RTC is quite inaccurate and has low resolution. Other time sources have better resolution and hence easy to discipline.
Most systems now read RTC at startup and then use other time sources.

# Use of timers
## clock source
Provides basic timeline. Should be monotonically increasing, non-stop, uniform timer. It is used for system clock hence should be high resolution, stable and correct.
`cat /sys/devices/system/clocksource/clocksource0/available_clocksource`
`cat /sys/devices/system/clocksource/clocksource0/current_clocksource`
`cat /proc/timer_list`
`cat /proc/timer_stats # controlled via CONFIG_TIMER_STATS`
## clock events
They take snapshot of timeline and interrupt at certain point, providing higher resolution. Usually APIC timers are used for it.
## clock scheduling

# gettimeofday vs clock_getttime
Both give nanoseconds precision but with `clock_gettime` one can specify clock source.
`gettimeofday` is not guaranteed to be monotonic, e.g. due to ntp it can go back in time. 
`clock_gettime` with `CLOCK_MONOTONIC` guarantees monotonicity though.

# Jiffies and tickless
Clock source keeps time as counters called jiffies. Jiffies get incremented by 1 at each timer interrupt.
Earlier, this is how all clocks in Linux used to keep time, but now it is deprecated and decoupled from timer management.
TSC and HRT are used on most systems.
`CONFIG_NO_HZ` option allows for tickless, ie where timeslice for scheduling is controlled by scheduler instead of jiffies.

# Adjusting time
## stepping
Make one discontinous change to adjust clock (e.g. with `settimeofday`)
## Slewing
Make the frequency faster or slower for certain time (`adjtimex` uses this)
## Smearing / Fudging
Gradually add large chunks of time over a period (e.g. in kernel `hardpps`)
## NTP

struct smp_ops smp_ops = {
    .cpu_up = native_cpu_up
}

smp_init
 cpu_up = native_cpu_up
 check_tsc_sync_source
  check_tsc_warp
