https://geidav.wordpress.com/2014/04/27/an-overview-of-direct-memory-access/

Prior to DMA, PIO (programmable I/O) was used where single bus connected CPU and memory and other devices.
If a device needed to process some memory, CPU reads it from memory to save it in registers and then transfers to device.
This wastes time as well as CPU cycles, since CPU was not processing data at all.

A DMA frees CPU cycles by direct transfer between memory and device.
A single DMA on bus can directly transfer data from memory to device. But since CPU is also controlling bus, there needs a arbitrator.
CPU and DMA controller connect to arbitrator via REQ/GRANT signals, which grants access to master bus.
Earlier (with ISA bus) use to be single DMA controller (intel 8237) but with PCI bus, DMA contoller is obsolete with bus mastering technique.
Device with DMA engine can 'master' bus and access memory directly but it needed arbitration mechanism to allow only one master.
With PCIe (p2p structure), full duplex DMA transfers of multiple devices. All arbitration gets replaced by packet routing logic in PCIe switches.

The DMA still needs to know when and where to transfer data. This happens under software control from CPU.
Before that, DMA could be either connecting to device via system bus (external DMA) or device is behind DMA (so, device is not memory mapped or visible to CPU).
When CPU finds out that transfer is to be done between device and memory, it configures DMA with -
 * starting address where data is located
 * starting address where data is to be saved
 * total length of data
Finally, CPU sets DMA to start. Once DMA is done, either CPU polls status register or gets an interrupt.
If device was behind DMA, instead of device address, CPU sets up DMA direction.

Process sess continous memory but DMA sees non-contiguous pages.
So DMA can transfer only one page at a time, had it not due to scatter/gather.
Scatter/gather list accumulates all addresses which are non-contiguous.
IOMMU is MMU for IO, where it helps creates virtually contiguous memory.

In a typical DMA transfer:
 - an event (e.g. incoming data on UART) notified DMA controller that data needs to be transferred to memory
 - DMA controller asserts 'DMA request' signal to CPU, asking permission to use the bus
 - CPU completes its current bus activity, stops driving bus, and returns 'DMA ack' signal to DMA controller
 - DMA controller reads and writes one or more memory bytes, driving the address/data/control signals as if it were CPU.
 - (CPU's address, data and control outputs are tristated while DMA controller controls)
 - When transfer is complete, DMA controller stop driving bus and deasserts DMA request signal. 
 - CPU removes its DMA ack signal and resumes control of bus

# Addresses
Physical: CPU untranslated. This is what CPU sees for accessing memory
Virtual : CPU translated. Internal to CPU, gets translated before RAM access
Bus     : Address of memory seen by other device (DMA address) dma_addr_t
```
virt ->[mmu]-> phys
bus ->[iommu]-> phys
```
Earlier `virt_to_bus()` and `bus_to_virt()` were used. Since DMA regions was strictly fixed, these functions provided static scheme translation. 
Now with new architecture, IOMMU allows for dynamic DMA window. The new API still uses `virt_to_bus()` underneath, where hardware doesn't has IOMMU.

# Memory allocation for DMA
DMAable memory means, some memory which can be somehow accessed by a DMA engine.
Similarly, DMA buffer means memory buffers which are source or destination of DMA transfer.

## `dma_alloc_coherent()``
allocates non-cached physically contiguous memory
Since it's non-cached, CPU and IO device can see same contents
Also, no cache flush/invalidate involved hence faster

## `dma_alloc_noncoherent()`
Usually not implemented

## `pci_alloc_consistent()`
Used to allocate DMAable memory. It is meant for permanent allocations such as rx/tx rings.
It is not suitable for short-lived skbuffs. Use `dma_map_single()` for skbufs.
`pci_alloc_consistent()` returns two address - virtual and bus. 
Note, bus address could be different than physical addr (e.g. with IOMMU)

## `dma_map_single() / dma_unmap_single()`
Used for short-lived skbufs.
Transfers ownership of a buffer from CPU to DMA hardware.
unmap transfers ownership from DMA to CPU.

Note: a system is 'consistent' when it is both coherent (each memory location has one and only one value at any given time) as well as no reordering in memory accesses.
While `pci_alloc_consistent()` is coherent, reodering can still happen. Hence, it is more of coherent rather than consistent.

To allocate memory, in early Linux separate memory was saved for DMA. Later in kernel, using `io_remap()` variations this saved memory was mapped into kernel's page tables.
Recently, continous memory allocator (CMA) can be used to allocate larger amount of memory, which can only be accessible by `dma_alloc_coherent()`.

DMA can in either case
1. If software asks for data (via func such as read)
   1. When process calls read(), driver method allocates DMA buffer and tells hardware to transfer its data into that buffer.
   1. Process is put to sleep.
   1. Hardware writes data to DMA buffer and raises interrupt when it's done.
   1. The interrupt handler, gets input data, acks the intr, wakes up process
   1. process reads the data.
1. Hardware asynchronously pushes data to the system
   1. Hardware raises intr to announce arrival of new data
   1. Interrupt handler allocates buffer and tells hardware where to put data
   1. The device writes the data into buffer and raises another intr when done
   1. Handler dispatches new data, wakes any relevant process, does housekeping

NICs use #2. They usually allocate DMA ring buffer. Each incoming packet is
placed in the next available buffer ring and interrupt is raised. The driver
then passes the packets to rest of the kernel and places a new DMA buffer in
the ring.

DMA API can use address returned from kmalloc, `_g_f_p*()`, `kmem_cache_alloc()` but
not from vmalloc, kmap. (although it can use vmalloc's address but need to find
phys page first).

Block I/O and networking subsystem make sure that buffers they use are DMA'able.

By default, kernel assumes the device can drive 32 bits on PCI bus for 
addressing. For 64 or 24 etc, device driver needs to specifically tell.
pci_set_dma_mask() is used to set this mask. If successful, PCI layer saves
this mask for later use.

pci_alloc_consistent() by default returns 32 bit address. So if device using
any other, use pci_alloc_consistent_dma_mask().

There are two types of DMA mappings -
1. consistent: Usually mapped at driver init and unmapped at end. Hardware
   guarantees that CPU and device can access data in parallel. Will see updates
   made by each other without explicit software flush.
   Note, CPU may still reorder stores to consistent memory. So, on some arch
   one might have to use wmb between writes.
   e.g. network DMA ring descriptors use it.
        dma_addr_t dma_handle;
        va = pci_alloc_consistent(pdev, size, &dma_handle);
   or if device is not PCI,
        va = dma_alloc_coherent(dev, size, &dma_handle); // non-cached physically contiguous
   returns two addr - va for CPU and dma_handle for device.
   Due to "coherency" there is performance penalty. 
2. streaming: Usually mapped for one DMA transfer and unmapped right after it.
   Hardware optimizes for sequential access.
   e.g. network card tx/rx buffers use it.
   Map a single region:
        dma_handle = pci_map_single(pdev, addr, size, direction);
      When DMA activity is done, call pci_unmap_single. eg. from interrupt
      handler which notifies DMA transfer done.
      Also, instead of page start, page offset can be mapped too.
        dma_handle = pci_map_page(pdev, page, offset, size, direction);
   Map a scatterlist:
        count = pci_map_sg(pdev, sglist, nents, direction);
        struct scatterlist *sg;
        for_each_sg(sglist, sg, i) {
            hw_address[i] = sg_dma_address(sg);
            hw_len[i] = sg_dma_len(sg);
        }
      Where, nents is num of entries in scatter gather list. 
  Faster, as do not guarantee consistency. When streamed buffer has been
  mapped for device access, driver has to explicity unmap (or sync) it before
  the CPU can reliably operate on it. 
  pci_map_{single|sg} map, unmap and synchronize single/sg preallocated DMA
  buffer.?
  Right before DMA transfer from/to RAM, pci_dma_sync_single_for_{device|cpu}
  can be invoked, which flushes/invalidates cache lines.
  Even buffer in high memory can be used for DMA transfers, with pci_map_page.

For streaming mapping, driver also needs to specifu direction for DMA transfer.
In consistent mapping, direction is set to PCI_DMA_BIDIRECTIONAL. E.g. in 
network tx buffer, PCI_DMA_TODEVICE is used. PCI_DMA_FROMDEVICE in rx.

Coherent/consistent mappings are simple but expensive to use.
Coherent mapping preferred when both CPU and device need to frequently access
 the DMA buffer.
Streaming mapping useful when device owns buffer for long duration.
Streaming mapping common for async operation when each DMA operates on different
 buffer. e.g. skbufs
Coherent mapping useful for DMA descriptors, which cotain metadata about DMA
 buffer.


Exact rules of when coherency is established is as follows -
op    | PCI_DMA_TODEVICE       | PCI_DMA_FROMDEVICE  | PCI_DMA_BIDIRECTIONAL  |
------+------------------------+---------------------+------------------------+
map   | makes CPU data visible | no effect           | makes CPU data visible |
unmap | no effect              | makes dev data visi | makes dev data visible |
sync  | makes CPU data visible | makes dev data visi | invalid                |

Cache coherency
----------------
If the DMA buffer is cached, it could cause inconsistency between device vs 
CPU's view of a location. Solution is to -
a. mark DMA memory uncacheable
b. OS handles flush/invalidate
c. hardware supports cache coherency
