
Demand paging or paging or swapping is storing some pages in media instead of in RAM and hence, freeing up memory.
It's useful - when system is running short of memory, and swapping out pages from initial bootup which are never needed again.
Since pages are backed to some media, cost of swap is very high.

Kernel pages cannot be swapped out. Any page which is pinned or locked is also not swapped.
However, anonymous or private pages (including from heap) which are not file mapped can be swapped.
If too much of swapping occurs, particulalrly swap-in and swap-out, system is `thrashing`.
To avoid thrashing, kernel needs to identify pages which are least used instead of working pages.
It uses 'second chance' or 'LRU' algorithms to pick which pages to swap-out.

