# Translation Lookaside Buffer
Used as cache for translating virtual address to physical address. Usually VA to PA involves page walk of page table. However, that takes time since it involves multiple accesses to RAM (since pages tables are in RAM). TLB saves these translations for faster lookups.

TLB is fully-associative or n-way associative cache. It takes 1 clock cycle for hit. To be fast, TLBs need to be small (64 entries 4-way 4Kb pages, 32 entries 4-way 2MB pages, etc). There are separate TLBs for instructions (iTLB) and data (dTLB) as well as multiple levels of TLBs just like L1,L2 caches.

                          VPN          offset
Virtual address       [31 ....... 12][11 .... 0]
                          vv              vv
                   <<TLB or page walk>> <<as is>>
                          vv              vv
                      [   PPN       ][ offset  ]

Since TLBs are small, bigger page size will have better efficiency. Also on TLB miss, hardware can do page walk itself and fill TLB entry. For this, page tables stored in RAM should be in specific way, supported by hardware.

# TLB and caches
## Physically indexed
CPU -> VA -> TLB -> PA -> cache
Slow since must do TLB lookup before accessing the cache.
## Virtually indexed
CPU -> VA -> cache -> TLB -> PA
Fast since TLB lookup only when cache miss. But two different processes on same CPU will collide with same virtual address. Two alleviate that, it can either -
1. flush all TLB on context switch, or
2. put some tag in cache which is specific to a process

Best is to do lookup in cache using VA and verify data with a PA tag. This way cache lookups and page walk can be done in parallel. Also, called VIPT (virtually indexed, physically tagged)
                  [cache] -> PA tag
                  /                 \
cpu -> VA [VPN][offset]             [cache hit or miss]
             \                       /
              [TLB] -> PA  ->  PPN /

# TLB management in Linux
INVPCID instruction is used to invalidate TLB entry based on process context ID.
1. `flush_tlb_all(void)`
Most severe, flush all TLB entries. Usually invoked when kernel page tables are changed.
2. `flush_tlb_mm(struct mm_struct *mm)`
Flushes entire process address space from TLB. Usually invoked to handle whole process address space change, such as in `fork()` and `exec()`.
3. `flush_tlb_range(struct vm_area_struct *vma, unsigned long start, unsigned long end)`
Flush only specific range of virtual address for a process.
4. `flush_tlb_page(struct vm_area_struct *vma, unsigned long addr)`
Flushes PAGE_SIZE sized translation from TLB. Used during fault processing.
5. `update_mmu_cache(struct vm_area_struct *vma, unsigned long address, pte_t *ptep)`
At the very end of page fault, this routine is invoked to tell kernel that translation now exists in page table for address. Kernel may use this to preload TLB translation like in SPARC64. On x86, hardware does it already.

```
sys_munmap                                include/linux/syscall.h
 vm_unmap                                 mm/mmap.c
  do_munmap
   unmap_region
    tlb_finish_mmu                        mm/tlb.c
     arch_tlb_finish_mmu
      tlb_flush_mmu_range
       flush_tlb_mm_range                 arch/um/kernel/tlb.c
        native_flush_tlb_others           arch/x86/mm/tlb.c
         smp_call_function_many           kernel/smp.c
          smp_call_function_single
           generic_exec_single
            native_send_call_func_single_ipi
```
Once IPI is generated on a CPU
```
call_function_single_interrupt            arch/x86/kernel/idt.c
 smp_call_function_single_interrupt       arch/m32r/kernel/smp.c
  generic_smp_call_function_single_interrupt kernel/smp.c
   flush_smp_call_function_queue
    flush_tlb_func_remote                 arch/x86/mm/tlb.c
     flush_tlb_func_common
```

# TLB shootdown
There is bus snooping for TLBs, so when one CPU invalidates or flushes some TLB entry, how does other CPU (running process from same context, e,g another thread) know that some TLB entries are no more valid? When say `free()` occurs on one CPU, kernel removes mappings for these pages along with invalidating the local TLB. Then it raises an IPI targeted for other CPU, which pauses other CPU and then invalidate certain TLB entries before resuming. In Linux, `smp_call_function_many()` allows a function to be called on other CPUs and used here to invalidate TLBs using function `flush_tlb_func_remote()`. All this is encapsulated within `native_flush_tlb_others()`.
