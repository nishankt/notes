VFS or virtual filesystem is a shim layer between kernel APIs and underlying storage drivers layer.
```
  +--------------+
  | Application  |
  +--------+     +
  |  glibc |     |
 -+--------------+-
  | system call  |
  +--------------+
  | VFS          |   icache, dcache
  +--------------+
  | fs(ext,sysfs)|   buffer cache
  +--------------+
  | block device |   I/O scheduler
  +--------------+
  | drivers      |
 -+--------------+-
  | disks        |
  +--------------+
```
Following are four main "objects" of VFS layer. Each has a set of method (*_operations) associated.
# superblock
  This struct/object represents a specific mounted filesystem. It is both on disk and in memory.
```c
  struct super_block {
    //blocksize, dirty flag, max file size, filesystem type, fs magic number, ref count, list of inodes/dirty indeos/writeback inodes, list of assigned files, list of dentries
  }
  struct super_operations {
    //alloc_inode, destroy_inode, write_inode, delete_inode, write_super, sync_fs, freeze/unfreeze, statfs, remount_fs
  }
```

# inode
  Represents a specific file within a filesystem. It is both on disk and in memory.
```c
  struct inode {
    //list of inodes, list of superblocks, list of block devs, inode number, ref count, uid/gid, real device node, file size (in bytes and blocks), driver info, inotify
  }
  struct inode_operatiions {
    // create, lookup, link, unlink, symlink, mkdir/rmdir, mkdnod, rename, truncate
  }
```

# dentry
  Represents each components in the specific path. e.g. /mnt/foo.txt then there is a dentry for /, mnt, and foo. It is only in memory structure, no "dirty" fields.
  A valid dentry object can be found in 3 states - used (valid inode, refcnt > 0), unused (valid inode, refcnt = 0), negative (invalid inode, refcnt less than 0).
```c
  struct dentry {
    // ref count, inode associated, flags, hash, mounted?, name, lru, subdirs, superblock of file
  }
  struct dentry_operations {
    //hash, revalidate, compare, delete, release
  }
```
  Since dentry lookup can be quite expensive, a cache is maintained - `struct dcache`. Dentry cache has 3 parts -
  1. list of used dentries
  1. LRU in form of doubly linked list of unused and negative dentries
  1. Hash table and hash function to quickly resolve path into dentry object

# file
  Represents an inode which is opened by a process. Only in-memory structure.
```c
  struct file {
    // path (contains dentry), object usage count, flags, mode, page cache mapping, epoll links
  }
  struct file_operations {
    // llseek, read, write, aio_read/write, readdir, poll, ioctl, open, release, mmap, fsync, flush, sendfile, sendpage
  }
```

There are other related data structures which are overall or per-process.
```c
  struct file_system_type; // every fs type registers here - describes fs capabilities like get_sb(), kill_sb(), module owner
  struct vfsmount;         // created for every mounted fs. Contains mntpnt, dentries, root, parent, devname, etc
  // following are per-process, so also part of in task_struct
  struct files_struct;     // per-process. Contains array of 64 fd pointing to `struct file`. For 64+ opened files, allocate another field.
  struct fs_struct;        // per-process. Contains root dir, cwd for the process.
  struct mnt_namespace;    // per-process. namespace for process. If not default ns, different root.
```

