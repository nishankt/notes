While underlying device might be using "sectors" of varying sizes, Linux uses "block" as a basic entity to communicate with underlying device. Block size is typically 512b, 1k or 4k (which means it might contains multiple sectors).
Flash devices also use "block" instead of "sectors" but they are different from Linux's notion of blocks.

# Buffer
When a block is stored in memory (either post-read or pre-write), it is stored in `buffer`. Each buffer contains exactly one block.
Every `buffer` is associated with a descriptor called `buffer_head`, which contains information about block as well as underlying device for this block.
```c
  struct buffer_head {
    // buffer state flag, [associated page, offset for data in page, size of mapping], [starting block number, associated block device], I/O completion, refcnt
  }
```
`buffer_head`'s state flag tell if the `buffer` is dirty, involved in some I/O request, currently doing disk I/O, mapped to on-disk block, contains valid data, etc. It acts as mapping between physical block and page containing in-memory `buffer`.

Prior to 2.6 kernel, `buffer_head` was a big struct acting as map between page and buffer, as well as unit of I/O through block layer and filesystem. Now it is only used to extract block mapping within a page (to track state) and to wrap `bio` submission for backward compatility. `bio` is used as basic I/O unit.

# bio
This is the basic container of block I/O operations in kernel. It's primary purpose is to represent every block I/O operation in flight (i.e. active) as list of segments. A segment is a contiguous memory of a buffer. That is individual buffer need not be contiguous itself, even though block on device is. This supports scatter-gather operation.
```c
  struct bio {
    // assoc sector on disk, list of reqs, assoc block dev, command flags, read vs write op, nr segments, sizeof first/last segment, bio_vec list, nr bio_vecs, cur idx in bio_vec
  }
  struct bio_vec {
    // ptr to page on which buffer resides, length of this buffer, offset in page where buffer resides
  }
```

`struct bio` contains pointer to array of `struct bio_vec`, with first entry at `bi_io_vec`. Each element in array is a pointer to a page containing buffer. The array is of size `bio_vcnt` and `bi_idx` is current index in this array.

# Request queue
Each block device maintains a `struct request_queue` for pending I/O requests. Each request in queue is of type `struct request`, which is composed of one or more `bio`.

# I/O schedulers or elevators
Instead of sending request to device immediately, kernel batches them and does sorting and merging of requests, to improve performance. I/O schedulers' job is to perform these batching and merging prior to sending the requests. It does so by managing block device's request queue.
`/sys/block/sda/queue/scheduler` contains scheduler in use.

## Noop I/O scheduler
   Just does merging and no sorting. Intended for random-access devices.
## Anticipatory I/O scheduler
   Works best for servers but poorly on typical database server.
## CFQ I/O scheduler
    Difficult to predict performance and depends on usecase.
## Deadline I/O scheduler
    Difficult to predict performance and depends on usecase. Might add latency. Best to try on specific workload to check if deadline vs CFQ is better.

