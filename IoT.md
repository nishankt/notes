
# Main components
- Hardare devices
- Connectivity
- Product Cloud
- Identity and security

# Hardware
  - Sensors - temp, proximity, pressure, gas, smoke, moisture, ultrsonic, ..
    1. BME280 (mainly for temp, but also senses pressure and humidity)
  - Micro-controller based
    1. Arduino - great for prototyping, lot of pins, easy to develop
    1. ESP8266/ESP32 board - onboard WiFi & BT, low cost, low power (support for deep sleep), 
    1. STM32F board - complex but production friendly
  - Microprocessor based
    1. Raspberry Pi - Linux or Windows, easy development
    1. Beaglebone - open source, Android or Linux, built-in flash
  - Software framework
    1. Arduino framework
    1. FreeRTOS
    1. Amazon FreeRTOS
    1. Apache Mynewt
## Dev kits
1. ESP32 WROOM devkit

# Connectivity
  - Communication technology
    1. WiFi: good for indoor devices
    1. RFID/NFC: good for card based access
    1. GSM/GPRS: outdoor standalone devices
    1. Bluetooth: choice for wearables and devices controlled via phone
    1. LoRaWAN: for industrial and public infra products in 3-5km range
    1. NB-IoT: low power narrow band cellular communication
  - Communication protocol
    1. HTTP
    1. Websockets
    1. MQTT
    1. AMQP

# Cloud / backend
  Azure, GCP, AWS, Watson all provide IoT focused products

