# Memory ordering
On a uniprocessors, load and store are in same order as specified by program. Also called *program order*.

# Memory coherence
Ordering of operations from different processors to *same* location.

## Snoopy bus
Bus based, single point of serialization for all memory requests. Processors observe other processors actions.

## Directory
No shared bus but caches talk to a middle man called directory. Directory knows which processors have which cache block. Can be a distributed directory, which can use partitioning.

# Memory consistency
Ordering of *all* memory operations from different processors. In other worlds, global ordering of accesses to all memory locations. However, primarily concerned for different memory location as same location is covered by coherence model.

## Sequential consistency
Not used in practice.
All memory operation are atomic and no reordering
It is simple but non-performant since many times atomicity is not needed across instructions, when not working on same location or dependency.

## Total Store Order (TSO)
Used in Intel, AMD, SPARC
Following reodering not allowed - W-W, R-R, W-R.
But R-W reorder might occur.

## Relaxed consistency
Used in ARM, Tilera

## Virtualized
Could have different consistency model than underlying machine.

# Consistency
Using barriers - lfence, sfence, mfence
Using locks and synchronization

# Compiler reordering
Could reorder instructions

